BEGIN{
    author="Dmitri Lapin <dmitri.lapin@gmail.com>"
    package="gitlab-tracker"
}

function signature( author, date ) {
    if(date) { print "\n -- " author "  "date "\n\n"}
}

/^\s*$/{ next }
/^##\s+\[[0-9.]+\]\s+-\s+[0-9]{4}-[0-9]{2}-[0-9]{2}$/{
    signature(author, date)
    if(match($0, /\[[0-9.]+\]/)){
        version = substr($0, RSTART + 1, RLENGTH - 2);
    }
    if(match($0, /[0-9]{4}-[0-9]{2}-[0-9]{2}/)){
        date = substr($0, RSTART, RLENGTH);
        command = "date -R -d " date
        command | getline date
    }
    print package " (" version ") stable; urgency=medium\n";
}
!/^##\s+\[[0-9.]+\]\s+-\s+[0-9]{4}-[0-9]{2}-[0-9]{2}$/{
    if(match($0, /^#+[ ]+/)) { $0=substr($0, RLENGTH + 1)}
    if(version) { print "  " $0}
}
END{
    signature(author, date)
}