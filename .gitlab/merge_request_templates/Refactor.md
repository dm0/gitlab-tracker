## Refactoring details
<!-- Briefly describe what this MR is about -->

## Related issues
Resolves #XXX.

## Author's checklist
- [ ] Source branch is `refactor/*`.
- [ ] Target branch is `develop` or `release/*` or `feature/*`.
- [ ] MR title includes issue action if relevant ("resolves #xxx").
- [ ] Changes are described in the changelog (unreleased section).
- [ ] Contains isolated set of changes related to the refactoring.

## Post-merge actions
- [ ] Move issue to the ~Merging column.


/label ~Refactor
/target_branch develop

