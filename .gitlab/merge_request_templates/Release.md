## Changes
<!-- Briefly describe features and fixes of the new version -->

## Version tag
Version `vX.Y.Z`.


## Author's checklist
- [ ] Source branch is `release/*`.
- [ ] Source branch name contains version information.
- [ ] Target branch is `master`.
- [ ] Changes are described in changelog and have correct header.

## Post-merge actions
- [ ] Assign version tag to the merge commit.
- [ ] Marge `master` manually into `develop` (with merge commit).
- [ ] Update labels of the automatically closed issues.  
      Closed issues should not have ~"To Do" ~Doing or ~Merging labels


/label ~Release
/target_branch master

