## Bug details
<!-- Briefly describe what this MR is about -->

## Related issues
Resolves #XXX.

## Version tag
Version `vX.Y.Z`.

## Author's checklist
- [ ] Source branch is `hotfix/*`.
- [ ] Target branch is `master`.
- [ ] MR title includes issue action if relevant ("resolves #xxx").
- [ ] Changes are described in changelog and have correct header.
- [ ] Contains isolated set of changes related to the hotfix.

## Post-merge actions
- [ ] Assign version tag to the merge commit.
- [ ] Marge `master` manually into `develop` (with merge commit).
- [ ] Update labels of the automatically closed issues.  
      Closed issues should not have ~"To Do" ~Doing or ~Merging labels


/label ~Hotfix
/target_branch master
