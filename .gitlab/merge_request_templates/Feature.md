## Feature details
<!-- Briefly describe what this MR is about -->

## Related issues
Resolves #XXX.

## Checklist
- [ ] Source branch is `feature/*`.
- [ ] Target branch is `develop` or `release/*`.
- [ ] MR title includes issue action if relevant ("resolves #xxx").
- [ ] Contains isolated set of changes related to the feature.
- [ ] Changes are described in the changelog (unreleased section).

## Post-merge actions
- [ ] Move issue to the ~Merging column.


/label ~Feature
/target_branch develop

