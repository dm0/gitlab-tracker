# GitLab Tracker

GitLab Tracker is a time tracking desktop application for GitLab.

It uses GitLab API to fetch projects, issues and time estimate and allows to set time estimate and to track time spent on an issue.

## Features

Current version of the application can:

* Request GitLab authentication from the user.
* Fetch projects user has write access to.
* Fetch open issues of the selected project.
* Set time estimate of an issue.
* Track time of an issue.
* Add manual time for an issue.
* View time records of an issue.
* Revert selected time record.
* Hide itself to a system tray icon.

### User interface

### Time tracking
<img src="docs/images/time-tracking-ui.png" width="400"/>
<img src="docs/images/time-tracking-overdue.png" width="400"/>

### Time records
<img src="docs/images/time-records.png" width="400"/>

### Time estimate and manual time
Time input dialog supports free form or control-based input.

<img src="docs/images/time-estimate.png" width="500"/>

<img src="docs/images/manual-time.png" width="500"/>

<img src="docs/images/manual-time-free-form.png" width="500"/><br/>

### Tray menu
<img src="docs/images/tray-menu.png" width="500"/>

### Application settings
<img src="docs/images/settings-dialog.png" width="500"/>

## Installation

Pre-built binary packages for Ubuntu 18.04 can be downloaded from the [Releases](/../-/releases) page.

Downloaded package can be installed using `dpkg`:
```
sudo dpkg -i gitlab_tracker.deb
```
Followed by
```
sudo apt-get install -f
```
to fix possible missing dependencies.

### Build instructions

Application uses QtNetworkAuth module for oAuth authentication. This module is 
not available in Ubuntu 18.04. Thus to build this application in Ubuntu 18.04 
you either need to build and install this module or to use Qt Creator's update 
mechanism to install Qt toolkit and this module.

This repository includes Docker image with this module installed. This image 
can be used to build the application. See [Dockerfile](docker/ubuntu18.04-dev-env/Dockerfile) for details of how to build and install 
QtNetworkAuth module.

This project uses QMake build system. The following sequence of commands
executed in the project root builds and installs application.

```
qmake
make
sudo make install
```

### Build options
The following options can be specified at build time (with `qmake` invocation):

* `CONFIG` option `install_qnwauth` if set will install QtNetworkAuth library during `make install` step.  
  The main reason to have this option is to enable build of debian package 
  (`.deb`) and to make this package work on Ubuntu (it doesn't have this 
  module in the repository).
* `GITLAB_APP_ID` and `GITLAB_APP_SECRET` compile time constants must be 
  provided.  
  They should take values obtained after registering an application at GitLab.  
  These constants must be set this way:  
  ```
  qmake DEFINES+="GITLAB_APP_ID=appid" DEFINES+="GITLAB_APP_SECRET=appsecret"
  ```


## Other projects used in this project
1. [UGlobalHotkey](https://github.com/falceeffect/UGlobalHotkey) project, as a submodule. Used to setup global (system-wide) hotkeys.


## Project icon
Project icon is a composition of these two icons:
- `free vector stopwatch design elements 04` from https://freedesignfile.com/ (https://freedesignfile.com/39140-free-vector-stopwatch-design-elements-04/)
- GitLab logo from https://www.freepik.com/flatart

### Other icons used in the application
* https://www.iconfinder.com/iconsets/office-222
* https://www.iconfinder.com/iconsets/freecns-cumulus
* https://www.iconfinder.com/iconsets/smart-watch-glyph-filled
* https://www.iconfinder.com/iconsets/pictograms-vol-1
* https://www.iconfinder.com/iconsets/flat-actions-icons-9
* https://www.iconfinder.com/iconsets/tango-icon-library
* https://icons8.com