#-------------------------------------------------
#
# Project created by QtCreator 2019-03-03T15:23:28
#
#-------------------------------------------------

QT       += core gui network networkauth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gitlab-tracker
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 link_prl

SOURCES += \
    src/aboutdialog.cpp \
    src/asyncissuesmodel.cpp \
    src/asyncnotesmodel.cpp \
    src/asyncprojectsmodel.cpp \
    src/blockingprogress.cpp \
    src/main.cpp \
    src/settingsdialog.cpp \
    src/timeentry.cpp \
    src/timeutils.cpp \
    src/tracker.cpp \
    src/labelanimation.cpp \
    src/asyncjsonmodel.cpp \
    src/popupview.cpp \
    src/localtimerecords.cpp \
    src/wrapspinbox.cpp

HEADERS += \
    src/aboutdialog.h \
    src/appsettings.h \
    src/asyncissuesmodel.h \
    src/asyncnotesmodel.h \
    src/asyncprojectsmodel.h \
    src/blockingprogress.h \
    src/settingsdialog.h \
    src/timeentry.h \
    src/timeutils.h \
    src/tracker.h \
    src/labelanimation.h \
    src/asyncjsonmodel.h \
    src/popupview.h \
    src/localtimerecords.h \
    src/wrapspinbox.h

FORMS += \
        ui/aboutdialog.ui \
        ui/blockingprogress.ui \
        ui/settingsdialog.ui \
        ui/timeentry.ui \
        ui/tracker.ui

isEmpty(PREFIX): PREFIX = /usr

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = $$PREFIX/bin
!isEmpty(target.path): INSTALLS += target

# Qt NetworkAuth module (need to install it on Ubuntu 18.04)
qnwauth.path = $$PREFIX/lib
qnwauth.files = $${QT.networkauth.libs}/lib$${QT.networkauth.module}.so.*.*.*
install_qnwauth: INSTALLS += qnwauth

# Desktop integration
desktopfile.path = $$PREFIX/share/applications
desktopfile.files = data/gitlab-tracker.desktop
appicon.path = $$PREFIX/share/pixmaps
appicon.files = data/gitlab-tracker.png
INSTALLS += desktopfile appicon

# Gitlab Client Qt library
unix: LIBS += -L$$OUT_PWD/../third_parties/gitlab-client-qt/ -lgitlab-client-qt
unix: PRE_TARGETDEPS += $$OUT_PWD/../third_parties/gitlab-client-qt/libgitlab-client-qt.a


INCLUDEPATH += $$PWD/../third_parties/gitlab-client-qt/include
DEPENDPATH += $$PWD/../third_parties/gitlab-client-qt/include

RESOURCES += \
    resources/resources.qrc \
    resources/fallback-theme/fallback-theme.qrc

# macOS icon
ICON = resources/icons/appicon.icns
QMAKE_TARGET_BUNDLE_PREFIX=io.gitlab.dm0

STATECHARTS +=

# Versioning
include(../third_parties/qmakescm/git.pri)
QSCM_HEADERS += src/version.in

# UGlobalHotkey
include(../third_parties/uglobalhotkey/uglobalhotkey.pri)
