#include "tracker.h"
#include <type_traits>
#include <QApplication>

namespace {
  template<typename T>
  static auto test_set_fallback_theme(int)
  -> decltype(T::setFallbackThemeName(""), std::true_type{});
  template<typename>
  static auto test_set_fallback_theme(long) -> std::false_type;
} // namespace detail

template<typename T>
struct has_set_fallback_theme: decltype(test_set_fallback_theme<T>(0)){};


template<typename  T = QIcon,
         typename std::enable_if<has_set_fallback_theme<T>::value, int>::type = 0>
void try_set_fallback_theme(const QString& name)
{
    qDebug() << "Setting fallback theme to" << name;
    T::setFallbackThemeName(name);
}

template<typename T = QIcon,
         typename std::enable_if<!(has_set_fallback_theme<T>::value), int>::type = 0>
void try_set_fallback_theme(const QString&)
{
    qDebug() << "Can't set fallback theme (no framework support)";
}


int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("dm0");
    QCoreApplication::setApplicationName("gitlab-tracker");
    qSetMessagePattern("[%{time process}] %{function}: %{message}");


    QCoreApplication::setAttribute(Qt::ApplicationAttribute::AA_EnableHighDpiScaling, true);
    QCoreApplication::setAttribute(Qt::ApplicationAttribute::AA_UseHighDpiPixmaps, true);

    QApplication a(argc, argv);

    // Try set fallback theme if QIcon implementation has it (depends on the Qt version).
    try_set_fallback_theme("fallback-theme");
    if (QIcon::themeName().isNull()) {
        qDebug() << "Switching to fallback theme";
        QIcon::setThemeName("fallback-theme");
    }

    // Load and apply platform CSS.
    QFile stylesheet;
#ifdef Q_OS_MACOS
    stylesheet.setFileName(":/stylesheets/macos.qcss");
#endif
    if (stylesheet.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "Loading and applying stylesheet" << stylesheet.fileName();
        QTextStream stylesheet_stream(&stylesheet);
        a.setStyleSheet(stylesheet_stream.readAll());
        stylesheet.close();
    }

    Tracker w;
    w.show();

    return a.exec();
}
