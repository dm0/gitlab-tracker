#include "labelanimation.h"

LabelAnimation::LabelAnimation(const QString &animation_file, QObject *parent) : QObject(parent)
{
    movie = new QMovie(this);
    movie->setFileName(animation_file);
}

void LabelAnimation::animate(QLabel *target)
{
    target->installEventFilter(this);
    target->setMovie(movie);
    if (target->isVisible()) {
        start_animation();
    }
}

void LabelAnimation::start_animation()
{
    if (start_count == 0) {
        qDebug("Movie started");
        movie->start();
    }
    ++start_count;
}

void LabelAnimation::stop_animation()
{
    start_count = std::max(start_count - 1, 0);
    if (start_count == 0) {
        qDebug("Movie stopped");
        movie->stop();
    }
}

bool LabelAnimation::eventFilter(QObject */*obj*/, QEvent *event)
{
    switch (event->type()) {
        case QEvent::Hide:
            stop_animation();
            break;
        case QEvent::Show:
            start_animation();
            break;
        default:
            break;
    }
    return false;
}
