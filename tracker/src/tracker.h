#ifndef TRACKER_H
#define TRACKER_H

#include <QWidget>
#include <QComboBox>
#include <QAbstractButton>
#include <QTimer>
#include <QSettings>
#include <QProgressDialog>
#include <QSystemTrayIcon>
#include <QSortFilterProxyModel>
#include <QShortcut>

#include "uglobalhotkeys.h"

#include <gitlabclient/gitlabclient>

#include "asyncjsonmodel.h"
#include "asyncprojectsmodel.h"
#include "asyncissuesmodel.h"
#include "asyncnotesmodel.h"

#include "labelanimation.h"
#include "localtimerecords.h"
#include "timeentry.h"
#include "blockingprogress.h"
#include "settingsdialog.h"


namespace Ui {
class Tracker;
}

class Tracker: public QWidget {
    Q_OBJECT

public:
    explicit Tracker(QWidget *parent=nullptr);
    ~Tracker() override;


private slots:
    /**
     * A user selected different project in the combo box.
     * The combo box uses proxy model so this function maps it back to the source model internally.
     * @param index Index of the selected entry.
     */
    void project_chosen(int index);

    /**
     * A user selected different issue in the combo box.
     * The combo box uses proxy model so this function maps it back to the source model internally.
     * @param index Index of the selected entry.
     */
    void issue_chosen(int index);

    /**
     * Display message about network failure.
     * @param error Network error.
     */
    void network_failed(QNetworkReply::NetworkError error);

    /**
     * Toggle time tracking for the current issue.
     * @param active New state of the tracking.
     */
    void toggle_time_tracking(bool active);

    /**
     * Update UI related to time tracking.
     */
    void update_time_tracking_ui();


    /**
     * Update project selection combo to reflect current state of application.
     * Disable combo if currently tracking or if there is pending request.
     */
    void update_project_selection_ui();

    /**
     * Update issue / MR selection combo to reflect current state of application.
     * Disable combo if currently tracking or if there is pending request.
     */
    void update_issue_mr_selection_ui();

    /**
     * Start authorization process (oAuth2)
     */
    void request_authorization();

    /**
     * Initiate projects loading.
     */
    void fetch_projects();

    /**
     * Update local records widget visibility depending on if there are local records.
     */
    void update_local_time_record_visibility();

    /**
     * Update time stats for the entity (issue / MR).
     * @param project Project ID
     * @param type Entity type
     * @param id Entity ID
     * @param total_spent Total spent time (remote)
     * @param remote_time Remote (server) time
     */
    void local_time_synced(long project, LocalTimeRecords::Entity type, long id, long total_spent,
                           QDateTime remote_time);

    /**
     * Cancel tracking discarding currently tracked time.
     * @param confirm If cancel confirmation should be requested.
     */
    void cancel_tracking(bool confirm=true);

    /**
     * Add manual time (via time entry dialog).
     */
    void add_manual_time();

    /**
     * Set time estimate of current issue or MR.
     */
    void show_estimate_dialog();

    /**
     * Update time estimate of the active issue
     * @param estimate New time estimate, seconds
     */
    void set_active_issue_estimate(long estimate);

    /**
     * Display context menu on local or remote time records.
     * @param pos Event position.
     */
    void display_time_records_context_menu(const QPoint &pos);

    /**
     * Remove selected local time records.
     */
    void remove_local_time_records();

    /**
     * Revert selected remote time record.
     */
    void revert_remote_time_record();

    /**
     * Handle systray icon activation.
     * @param reason Activation reason.
     */
    void handle_systray_action(QSystemTrayIcon::ActivationReason reason);

    /**
     * Clear an re-populate recent issues menu.
     */
    void rebuild_recent_issues_menu();

    /**
     * Clear an re-populate recent projects menu.
     */
    void rebuild_recent_projects_menu();

    /**
     * Update recent issues menu to reflect currently selected (checked) issue.
     */
    void update_recent_issues_menu_checkstate();

    /**
     * Update recent issues menu to reflect currently selected (checked) issue.
     */
    void update_recent_projects_menu_checkstate();

    /**
     * A handler for the triggered signal of the recent issues menu.
     * @param activated Activated action
     */
    void handle_recent_issue_activation(QAction *activated);

    /**
     * A handler for the triggered signal of the recent projects menu.
     * @param activated Activated action
     */
    void handle_recent_project_activation(QAction *activated);

    /**
     * Save application state.
     *
     * Intended to be called to handle aboutToQuit signal.
     */
    void save_state();

    /**
     * Display overdue or about to overdue notification via systray balloon message.
     *
     * @param remaining Remaining time. Should be ≤ 0 if overdue.
     */
    void display_overdue_notification(long remaining);

    /**
     * Sync selected local time records.
     */
    void sync_local_time_records();

    /**
     * Toggle display of time records views.
     *
     * Saves / restores views on hide / show and toggles visibility.
     * @param show True to show or false to hide time records.
     */
    void toggle_time_records(bool show);

    /**
     * Open settings dialog.
     */
    void show_settings();

    /**
     * Handle global hotkey activation.
     * @param id Hotkey ID.
     */
    void handle_global_hotkey(size_t id);

    /**
     * Ask user if time estimate should be updated.
     */
    void suggest_estimate_changing();

    /**
     * hide application to system tray.
     */
    void hide_to_tray();

    /**
     * Restore application from system tray,
     */
    void restore_from_tray();

private:

    /**
     * Namespace for global HotKeys indices enumeration.
     */
    struct HotKeys {
        enum {
            TOGGLE_TRACKING = 1, /**< Index of toggle time tracking hotkey */
            TOGGLE_APPWINDOW /**< Index of toggle app window hotkey */
        };
    };

    /**
     * Setup combo box UI.
     * Performs the following:
     * - Setup combo box and button to be disabled when model updates.
     * - Adds action to the left position of combo box' that displays spinner animation
     *   during the model update.
     * - Make button initiate model refresh on click.
     * @param combo Combo box
     * @param refresh_button Refresh button
     * @param model Model related to combo box and button (normally it should be a combo box'
     * model, probably indirect).
     */
    void setup_async_combo_ui(QComboBox *combo, QAbstractButton *refresh_button,
                              AsyncJsonModel *model);

    /**
     * Close event handler.
     * Hides to system tray.
     * @param event Event object.
     */
    void closeEvent(QCloseEvent *event) override;

    /**
     * Hide event handler.
     * Change text of systray menu show / hide action.
     * @param event Event object.
     */
    void hideEvent(QHideEvent *event) override;

    /**
     * Show event handler.
     * Change text of systray menu show / hide action.
     * @param event Event object.
     */
    void showEvent(QShowEvent *event) override;

    /**
     * Event filter to handle events occurring in watched objects.
     * Currently watching:
     * - QLineEdit-s of project / issue combo boxes to select all text on focus-in event and
     *   deselect on focus-out events (?).
     * @param watched Watched object
     * @param event Event of the watched object.
     * @return Filtering status (true to stop event).
     */
    bool eventFilter(QObject *watched, QEvent *event) override;

    /**
     * Restore application state (saved in settings).
     */
    void restore_state();

    /**
     * Setup projects combo UI and UI-related models.
     */
    void setup_projects_ui();

    /**
     * Setup issues combo UI and UI-related models.
     */
    void setup_issues_ui();

    /**
     * Setup local and remote time records UI.
     */
    void setup_time_records_ui();

    /**
     * Setup GitlabClient connection and events.
     */
    void setup_gitlab_connection();

    /**
     * Setup time tracking UI and events.
     */
    void setup_time_tracking_ui();

    /**
     * Setup systray menu UI and events.
     */
    void setup_tray_menu();

    /**
     * Update project information string and tooltip in time entry dialog.
     * Fetches information about current project and issue and updates time entry dialog via
     * `set_project_info` accordingly.
     */
    void update_time_dlg_project_info();

    /**
     * Setup hotkeys for supported actions.
     * Setup signal bindings and calls `load_hotkeys_config`.
     */
    void setup_hotkey_signals();

    /**
     * Load hotkeys bindings.
     * Reads configuration from application settings and applies configured key sequences.
     */
    void load_hotkeys_config();

    /** Maximum time in seconds that should use seconds in format when auto_seconds enabled  */
    static constexpr int max_seconds = 600;

    Ui::Tracker *ui; /**< UI instance */
    gitlabclient::Client *client = nullptr; /** GitLab API client instance. */
    AsyncProjectsModel *projects_model = nullptr; /**< Model that lists projects. */
    AsyncIssuesModel *issues_model = nullptr; /**< Model that lists issues. */
    AsyncNotesModel *notes_model = nullptr; /**< Model that lists issue / MR notes (time records). */
    LabelAnimation *spinner_animation; /**< Spinner animation to be displayed on spinners */
    LocalTimeRecords *local_records; /**< Local (not synced) time records */
    int active_project; /**< ID of the selected project */
    QString active_project_title; /**< Title of the current project */
    int active_issue; /**< IID (issue number)  of the selected issue */
    long active_issue_time_estimate = 0; /**< Remote estimated time of the active issue */
    long active_issue_time_spent = 0; /**< Remote spent time of the active issue */
    QDateTime active_issue_info_time; /**< Remote time active issue info was updated */
    QString active_issue_title; /**< Title of the current issue */

    QDateTime tracking_start_time; /**< Time the tracking was started */
    QTimer update_tracking_ui_timer; /**< Timer for the time tracking UI update */
    QTimer switch_update_resolution_timer; /**< Timer to change update frequency */

    bool now_tracking = false; /**< If tracking activated */
    bool overdue = false; /**< Overdue status of the current issue */

    QSettings *settings; /**< Application settings */
    QIcon appicon; /**< Application icon */

    TimeEntry *time_dlg; /**< Time entry dialog */
    BlockingProgress *blocking_progress; /**< Blocking progress notification */
    QSystemTrayIcon *tray_icon; /**< System tray object */
    QMenu *time_tracking_menu; /**< Time tracking menu: set estimate, add manual time */
    bool time_tracking_menu_shown = false; /**< Track menu status to disable title updates */
    QSortFilterProxyModel *recent_issues_model; /**< Proxy model for recent issues */
    QMenu *recent_issues_menu; /**< A submenu with recent issues */
    int overdue_notification_time = 0; /**< A time to show notification before overdue, minutes */
    SettingsDialog *settings_dlg = nullptr; /**< Settings dialog instance */

    UGlobalHotkeys *global_hotkeys; /**< Global hotkeys mapper */
    QShortcut *focus_projects_shortcut; /**< Focus projects shortcut instance */
    QShortcut *focus_issues_shortcut; /**< Focus issues shortcut instance */

    QMenu *recent_projects_menu; /**< A submenu with recent issues */
};

#endif // TRACKER_H
