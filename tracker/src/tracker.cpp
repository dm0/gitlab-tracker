#include <QtGlobal>

#include <QSortFilterProxyModel>
#include <QCompleter>
#include <QLineEdit>
#include <QLabel>
#include <QWidgetAction>
#include <QMovie>
#include <QMessageBox>
#include <QMetaEnum>
#include <QJsonArray>
#include <QStringBuilder>
#include <QProgressDialog>
#include <QCloseEvent>
#include <QDesktopWidget>

#include <QDebug>
#include <cassert>

#include "tracker.h"
#include "ui_tracker.h"
#include "aboutdialog.h"

#include "asyncjsonmodel.h"

#include "labelanimation.h"
#include "popupview.h"

#include "timeutils.h"
#include "appsettings.h"

namespace glc = gitlabclient;

namespace keys = settings::keys;
namespace defaults = settings::defaults;

using timeutils::GitlabTimeAction;

#define Q(x) #x
#define QUOTE(x) Q(x)


Tracker::Tracker(QWidget *parent):
    QWidget(parent),
    ui(new Ui::Tracker),
    client(new glc::Client(QUOTE(GITLAB_APP_ID), QUOTE(GITLAB_APP_SECRET), this)),
    projects_model(new AsyncProjectsModel(nullptr, this)),
    issues_model(new AsyncIssuesModel(client, this)),
    notes_model(new AsyncNotesModel(nullptr, this)),
    spinner_animation(new LabelAnimation(":/animation/spinner", this)),
    local_records(new LocalTimeRecords(this)),
    settings(new QSettings(this)),
    appicon(":/icons/app-icon-128x128"),
    time_dlg(new TimeEntry(this)),
    blocking_progress(new BlockingProgress(this)),
    tray_icon(new QSystemTrayIcon(QIcon(":/icons/app-icon-32x32"), this)),
    global_hotkeys(new UGlobalHotkeys(this)),
    focus_projects_shortcut(new QShortcut(this)),
    focus_issues_shortcut(new QShortcut(this))
{
    ui->setupUi(this);

    // Restore state
    restore_state();

    // Setup projects & issues UI.
    setup_projects_ui();
    setup_issues_ui();

    // Setup local and remote time records UI.
    setup_time_records_ui();

    // Setup GitLab client connection and events handlers.
    setup_gitlab_connection();

    // Setup time tracking UI and events.
    setup_time_tracking_ui();

    // Setup systray menu UI and events.
    setup_tray_menu();

    // Setup hotkeys
    setup_hotkey_signals();
    load_hotkeys_config();

    // Save state before quitting
    connect(qApp, &QCoreApplication::aboutToQuit, this, &Tracker::save_state);

    // Do not quit on last window close
    qApp->setQuitOnLastWindowClosed(false);
}

Tracker::~Tracker()
{
    delete ui;
}

void Tracker::project_chosen(int index)
{
    QAbstractItemModel *combo_model = ui->projects_combo->model();
    // If index is -1 the selection was cleared due to the model update
    // We need to find active project in the loaded projects and try to keep selection
    // otherwise we reset active issue and project
    if (index < 0) {
        // Nothing to do if we have no active project
        if (!active_project) {
            return;
        }
        // Find active project in the fetched list.
        const int total_projects = combo_model->rowCount();
        for (index = 0; index < total_projects; index++) {
            if (active_project != combo_model->data(
                    combo_model->index(index, AsyncProjectsModel::Columns::ID)).toInt())
            {
                continue;
            }
            // Found previous project in the list
            const QSignalBlocker b(ui->projects_combo);
            ui->projects_combo->setCurrentIndex(index);
            break;
        }
        // Not found, reset active project
        if (index == total_projects) {
            active_project = 0;
            return;
        }
    }

    QModelIndex project_id_idx = combo_model->index(index, AsyncProjectsModel::Columns::ID);
    active_project = combo_model->data(project_id_idx).toInt();
    // Save active project
    settings->setValue(keys::active_project, active_project);

    // Move cursor to the beginning of the edit
    ui->projects_combo->lineEdit()->home(false);

    ui->issue_mr_combo->setEnabled(true);
    issues_model->load_project_issues(active_project);
    // Will be enabled after an issue selected
    ui->time_tracking_group->setEnabled(false);
    // Disable start tracking menu item
    ui->start_tracking_act->setEnabled(false);
    // Hide issue info action
    recent_issues_menu->menuAction()->setVisible(false);
    // Hide time tracking menu
    time_tracking_menu->menuAction()->setVisible(false);

    active_project_title =
        ui->projects_combo->model()->index(ui->projects_combo->currentIndex(),
                                           AsyncProjectsModel::Columns::NAME).data().toString();
    // Update tray icon tooltip
    tray_icon->setToolTip(active_project_title);

    // Update current project menu
    recent_projects_menu->setTitle(
        time_tracking_menu->fontMetrics().elidedText(
            active_project ? active_project_title : "Choose project",
            Qt::TextElideMode::ElideRight,
            settings->value(keys::max_menu_width,
                            defaults::max_menu_width).toInt()
            )
        );

    // Update recent projects list
    const int recent_count = settings->value(
        keys::recent_projects_count, defaults::recent_projects_count).toInt();
    const int num_saved = settings->beginReadArray(keys::recent_projects_list);
    QList<int> recent_projects;
    recent_projects.reserve(recent_count + 1);
    for (int i = 0; i < num_saved; ++i) {
        settings->setArrayIndex(i);
        recent_projects.append(settings->value("id").toInt());
    }
    settings->endArray();

    // If the list already contains the project, remove it
    recent_projects.removeAll(active_project);

    recent_projects.prepend(active_project);

    // Remove any extra items from the list
    if (recent_count > 0) {
        while (recent_projects.size() > recent_count) {
            recent_projects.removeLast();
        }
    }
    settings->beginWriteArray(keys::recent_projects_list, recent_projects.size());
    for (int i = 0, size = recent_projects.size(); i < size; ++i) {
        settings->setArrayIndex(i);
        settings->setValue("id", recent_projects[i]);
    }
    settings->endArray();

    // Update recent projects menu
    rebuild_recent_projects_menu();
    update_recent_projects_menu_checkstate();
}

void Tracker::issue_chosen(int index)
{
    qDebug() << "Issue selection changed" << index;
    QAbstractItemModel *combo_model = ui->issue_mr_combo->model();
    // If index is -1 the selection was cleared due to the model update
    // We need to find active issue in the loaded list and try to keep selection
    // otherwise we reset active issue and project
    if (index < 0) {

        // Project could change, load last active issue of the current project from settings.
        active_issue = settings->value(QString("%1/%2")
                                       .arg(keys::last_issues_group)
                                       .arg(active_project), 0).toInt();

        // Find active issue in the fetched list.
        const int total_issues = combo_model->rowCount();
        for (index = 0; index < total_issues; index++) {
            if (active_issue !=
                combo_model->index(index, AsyncIssuesModel::Columns::IID).data().toInt())
            {
                continue;
            }
            // Found previous issue in the list
            const QSignalBlocker b(ui->issue_mr_combo);
            qDebug() << "Setting index to" << index;
            ui->issue_mr_combo->setCurrentIndex(index);

            break;
        }
        // Not found, reset active issue
        if (index == total_issues) {
            qDebug() << "Issue" << active_issue << "not found in the list, resetting active issue";
            active_issue = 0;
            // Ask user what to do if currently tracking: Add tracked time or discard it.
            if (now_tracking) {
                const qint64 local_tracked =
                    tracking_start_time.secsTo(QDateTime::currentDateTime());
                int response = QMessageBox::question(
                    this,
                    "Active issue disappeared",
                    QString(
                        "The issue you were tracking has disappeared. "
                        "Would you like to commit tracked time (%1)?"
                        "\nChoose No to discard it."
                        ).arg(timeutils::format_time(static_cast<int>(local_tracked))),
                    QMessageBox::Yes | QMessageBox::No,
                    QMessageBox::Yes);
                if (response == QMessageBox::Yes) {
                    toggle_time_tracking(false);
                } else {
                    cancel_tracking(false);
                }
            }
        }
    } else {
        active_issue = combo_model->index(index, AsyncIssuesModel::Columns::IID).data().toInt();
    }

    active_issue_title = ui->issue_mr_combo->currentText();

    // Update tray icon tooltip
    tray_icon->setToolTip(QString("%1\n%2").arg(active_project_title).arg(active_issue_title));

    // Set time tracking controls state
    ui->time_tracking_group->setEnabled(!!active_issue);
    // Toggle start tracking menu item
    ui->start_tracking_act->setEnabled(!!active_issue);
    // Update issue info menu item
    recent_issues_menu->setTitle(
        recent_issues_menu->fontMetrics().elidedText(
            active_issue ? ui->issue_mr_combo->currentText() : "Choose issue",
            Qt::TextElideMode::ElideRight,
            settings->value(keys::max_menu_width,
                            defaults::max_menu_width).toInt()
            )
        );

    // Set time tracking menu visibility
    time_tracking_menu->menuAction()->setVisible(!!active_issue);

    // Exit if we have no current issue.
    if (!active_issue) {
        return;
    }



    // Save active issue
    settings->setValue(QString("%1/%2")
                       .arg(keys::last_issues_group)
                       .arg(active_project), active_issue);

    qDebug() << "Active issue now is:" << active_issue;

    active_issue_time_spent = combo_model->data(
        combo_model->index(index, AsyncIssuesModel::Columns::TIME_SPENT),
        AsyncIssuesModel::Roles::NUMERIC
        ).toInt();
    active_issue_time_estimate = combo_model->data(
        combo_model->index(index, AsyncIssuesModel::Columns::TIME_ESTIMATE),
        AsyncIssuesModel::Roles::NUMERIC
        ).toInt();

    // Update recent issues menu
    update_recent_issues_menu_checkstate();

    // Update the rest of time tracking UI.
    update_time_tracking_ui();

    // Move cursor to the beginning of the edit on next event loop iteration.
    QTimer::singleShot(0, this, [this]() {ui->issue_mr_combo->lineEdit()->home(false);});

    // If information was updated since last fetch -- reload data
    if (!active_issue_info_time.isNull()) {
        // Reset last info update time
        qDebug() << "Updating issues model (the list is outdated)" << active_issue_info_time;
        active_issue_info_time = QDateTime{};
        issues_model->refresh();
        return;
    }

    // Fetch notes (including time records)
    glc::AsyncJsonObjList *notes_list =
        client->notes()->list_issue_notes(QString::number(active_project), active_issue);
    notes_model->set_objects_list(notes_list);
}

void Tracker::network_failed(QNetworkReply::NetworkError error)
{
    // In case of AuthenticationRequiredError we need to start authentication procedure
    if (error == QNetworkReply::NetworkError::AuthenticationRequiredError) {
        request_authorization();
        return;
    }
    glc::AsyncJsonResponse *json_response = qobject_cast<glc::AsyncJsonResponse *>(sender());
    QString text = QString("Network request failed with error: %1")
                   .arg(QMetaEnum::fromType<QNetworkReply::NetworkError>().valueToKey(error));
    if (!json_response->json().isNull()) {
        text += "\nResponse:" + json_response->json().toJson();
    }
    QMessageBox::critical(this, "Network failure", text);
}

void Tracker::toggle_time_tracking(bool active)
{
    now_tracking = active;
    // We add space before text to fix icon-to-text padding.
    if (active) {
        tracking_start_time = QDateTime::currentDateTime();
        ui->start_stop_tracking->setIcon(QIcon::fromTheme("media-playback-stop"));
        ui->start_stop_tracking->setText(QLatin1Char(' ') % "Stop tracking");
        // Start timers.
        update_tracking_ui_timer.start(1000);
        switch_update_resolution_timer.start();
        tray_icon->setIcon(QIcon(":/icons/systray-tracking"));
    } else {
        ui->start_stop_tracking->setIcon(QIcon::fromTheme("media-playback-start"));
        ui->start_stop_tracking->setText(QLatin1Char(' ') % "Start tracking");
        // Stop timers.
        switch_update_resolution_timer.stop();
        update_tracking_ui_timer.stop();
        // Add record to the sync store.
        if (!tracking_start_time.isNull()) {
            local_records->add_record(
                active_project,
                LocalTimeRecords::Entity::Issue, active_issue,
                static_cast<int>(tracking_start_time.secsTo(QDateTime::currentDateTime())));
        }
        tray_icon->setIcon(QIcon(":/icons/app-icon-32x32"));
    }
    // Update time tracking actions
    ui->start_tracking_act->setVisible(!active);
    ui->stop_tracking_act->setVisible(active);
    ui->cancel_tracking_act->setVisible(active);
    recent_issues_menu->menuAction()->setEnabled(!active);
    recent_projects_menu->menuAction()->setEnabled(!active);
    // Update time tracking IU
    update_time_tracking_ui();
    // Update combo boxes
    update_project_selection_ui();
    update_issue_mr_selection_ui();
}

void Tracker::update_time_tracking_ui()
{
    long spent_time = active_issue_time_spent;

    // Update estimated time label.
    ui->estimated_time->setText(
        timeutils::format_time(active_issue_time_estimate, "Not set", max_seconds));

    QString tracking_state_msg;

    if (now_tracking) {
        const qint64 local_tracked = tracking_start_time.secsTo(QDateTime::currentDateTime());
        // Update local tracking timer
        ui->now_tracking_time->setText(
            timeutils::format_time(static_cast<int>(local_tracked), "0s", max_seconds));
        // Adjust total spent time
        spent_time += local_tracked;

        tracking_state_msg = "Now tracking: ";
    } else {
        tracking_state_msg = "Spent: ";
    }
    spent_time += local_records->get_local_tracked_time(
        active_project, LocalTimeRecords::Entity::Issue, active_issue);

    // Complete time tracking menu title & update menu
    tracking_state_msg += timeutils::format_time(spent_time, "0s", max_seconds);
    if (active_issue_time_estimate) {
        tracking_state_msg += QString(" / %1").arg(
            timeutils::format_time(active_issue_time_estimate, "", max_seconds));
    }
    // Only update title if menu is not shown (expanded)
    if (!time_tracking_menu_shown) {
        time_tracking_menu->setTitle(tracking_state_msg);
    }

    // Set tray icon tooltip message.
    // Compose of current project, issue and tracking state
    tray_icon->setToolTip(QString("%1\n%2\n%3")
                          .arg(active_project_title)
                          .arg(active_issue_title)
                          .arg(tracking_state_msg));

    ui->spent_time->setText(timeutils::format_time(spent_time, "Not reported", max_seconds));
    long remaining = active_issue_time_estimate - spent_time;
    bool has_remaining = active_issue_time_estimate;
    // -1 as max seconds results in seconds never present in the string.
    ui->remaining_time->setText(
        timeutils::format_time(std::abs(remaining), "less than a minute", -1));
    ui->remaining_label->setText(remaining > 0 ? "remaining" : "overdue");
    // Hide remaining time and label if no remaining time can be shown (no estimate)
    ui->remaining_time->setVisible(has_remaining);
    ui->remaining_label->setVisible(has_remaining);

    // Zero triggers busy mode.
    ui->time_progress->setMaximum(active_issue_time_estimate ? active_issue_time_estimate : 1);
    // Values above maximum are ignored.
    ui->time_progress->setValue(std::min(spent_time, active_issue_time_estimate));

    // Change color if overdue state changed.
    if (overdue != (spent_time > active_issue_time_estimate)) {
        overdue = spent_time > active_issue_time_estimate;
        QPalette p = palette();
        if (overdue) {
            p.setColor(QPalette::Active, QPalette::Highlight, Qt::darkRed);
            p.setColor(QPalette::Inactive, QPalette::Highlight, Qt::darkRed);
        }
        ui->time_progress->setPalette(p);
    }

    // Balloon notification. Show if time estimate set and about-to-overdue status changed.
    if (active_issue_time_estimate && overdue_notification_time > 0) {
        const long remaining = (active_issue_time_estimate - spent_time);
        const long overdue_notif_secs = overdue_notification_time * timeutils::seconds_per_min;
        // Current "about-to-overdue" state.
        const bool about_to_overdue = remaining <= overdue_notif_secs;
        // Previous "about-to-overdue" state (1 minute before).
        const bool about_to_overdue_1m =
            (remaining + timeutils::seconds_per_min) <= overdue_notif_secs;

        if (about_to_overdue != about_to_overdue_1m) {
            if (update_tracking_ui_timer.interval() > 1000 || remaining == overdue_notif_secs) {
                display_overdue_notification(remaining);
            }
        }
    }
}

void Tracker::update_project_selection_ui()
{
    bool disabled = projects_model->is_updating() || now_tracking;
    ui->projects_combo->setDisabled(disabled);
    ui->refresh_projects->setDisabled(disabled);
}

void Tracker::update_issue_mr_selection_ui()
{
    bool disabled = issues_model->is_updating() || now_tracking || projects_model->is_updating();
    ui->issue_mr_combo->setDisabled(disabled);
    ui->refresh_issues->setDisabled(disabled);
}

void Tracker::request_authorization()
{
    QMessageBox::information(
        this, "GitLab Tracker",
        "<p><b>This application requires GitLab API access.</b></p>"
        "<p>You need to confirm application access on the GitLab page that will be "
        "opened in the default browser.</p>");
    client->authorize();
}

void Tracker::fetch_projects()
{
    glc::AsyncJsonObjList *projects_list =
        client->projects()->membership(true).simple_response(true).list_projects();
    projects_model->set_objects_list(projects_list);
    connect(projects_list, &glc::AsyncJsonResponse::failed, this, &Tracker::network_failed);
}

void Tracker::update_local_time_record_visibility()
{
    ui->local_records_widget->setVisible(local_records->rowCount() > 0);
}

void Tracker::local_time_synced(long project, LocalTimeRecords::Entity type, long id,
                                long total_spent, QDateTime remote_time)
{
    // Wrong project, not an issue or a wrong issue: Do nothing.
    if (project != active_project || type != LocalTimeRecords::Entity::Issue
        || id != active_issue)
    {
        return;
    }
    // If current information is more up-to-date.
    if (!active_issue_info_time.isNull() && (active_issue_info_time > remote_time)) {
        return;
    }
    active_issue_info_time = remote_time;
    active_issue_time_spent = total_spent;
    update_time_tracking_ui();
    // Initiate time records refresh.
    notes_model->refresh();
}

void Tracker::cancel_tracking(bool confirm)
{
    // Ask confirmation if instructed
    if (confirm) {
        int response = QMessageBox::question(
            this, "Cancel tracking?",
            "This will stop tracking and discard any tracked time.\n"
            "Do you want to continue?",
            QMessageBox::Yes | QMessageBox::No,
            QMessageBox::No);
        if (response != QMessageBox::Yes) {
            return;
        }
    }
    // Stop tracking and discard tracked time.
    tracking_start_time = QDateTime();
    ui->start_stop_tracking->setChecked(false);
}

void Tracker::update_time_dlg_project_info()
{
    QModelIndex start_idx = projects_model->index(0, AsyncProjectsModel::Columns::ID);
    QModelIndexList found = projects_model->match(start_idx, Qt::DisplayRole,
                                                  QVariant::fromValue<long long>(active_project), 1,
                                                  Qt::MatchExactly);
    QString project_name, project_name_namespace, issue_name;
    if (found.size()) {
        project_name =
            projects_model->index(
                found.at(0).row(), AsyncProjectsModel::Columns::NAME).data().toString();
        project_name_namespace =
            projects_model->index(
                found.at(0).row(),
                AsyncProjectsModel::Columns::NAME_WITH_NAMESPACE).data().toString();
    }

    // Find issue name in issues model and set it in time entry dialog.
    start_idx = issues_model->index(0, AsyncIssuesModel::Columns::IID);
    found = issues_model->match(start_idx, Qt::DisplayRole,
                                QVariant::fromValue<long long>(active_issue), 1,
                                Qt::MatchExactly);
    if (found.size()) {
        issue_name =
            issues_model->index(found.at(0).row(),
                                AsyncIssuesModel::Columns::TITLE).data().toString();
    }

    QString tooltip = QString("<nobr><b>Project:</b> %1</nobr><br/><b>Issue:</b> #%2 — %3")
                      .arg(project_name_namespace)
                      .arg(active_issue)
                      .arg(issue_name);
    QString project_info = QString("%1 <b>#%2</b>").arg(project_name).arg(active_issue);

    time_dlg->set_project_info(project_info, tooltip);
}

void Tracker::setup_hotkey_signals()
{
    connect(focus_issues_shortcut, &QShortcut::activated,
            ui->issue_mr_combo, static_cast<void (QComboBox::*)()>(&QComboBox::setFocus));

    connect(focus_projects_shortcut, &QShortcut::activated,
            ui->projects_combo, static_cast<void (QComboBox::*)()>(&QComboBox::setFocus));

    connect(global_hotkeys, &UGlobalHotkeys::activated, this, &Tracker::handle_global_hotkey);
}

void Tracker::load_hotkeys_config()
{
    // Focus issues shortcut (local)
    focus_issues_shortcut->setKey(settings->value(keys::focus_issues_shortcut,
                                                  defaults::focus_issues_shortcut)
                                  .value<QKeySequence>());

    // Focus project shortcut (local)
    focus_projects_shortcut->setKey(settings->value(keys::focus_projects_shortcut,
                                                    defaults::focus_projects_shortcut)
                                    .value<QKeySequence>());

    // Toggle time tracking shortcut (global)
    try {
        global_hotkeys->registerHotkey(
            settings->value(keys::toggle_tracking_shortcut,
                            defaults::toggle_tracking_shortcut).toString(),
            HotKeys::TOGGLE_TRACKING);
        // Toggle application window shortcut (global)
        global_hotkeys->registerHotkey(
            settings->value(keys::toggle_appwindow_shortcut,
                            defaults::toggle_appwindow_shortcut).toString(),
            HotKeys::TOGGLE_APPWINDOW);
    } catch (UException e) {
        QMessageBox::critical(
            this, "Failed to register global hotkeys",
            QString("Global hotkeys registration failed with the following error: %1").arg(e.what()));
    }

}

void Tracker::add_manual_time()
{
    time_dlg->setWindowTitle("Manual time");
    time_dlg->set_header_text("Add manual time");
    time_dlg->set_time(0);
    time_dlg->set_button("Add", QIcon::fromTheme("add"));

    // Update project info in time entry dialog.
    update_time_dlg_project_info();

    if (time_dlg->exec() != QDialog::Accepted) {
        return;
    }
    long time = time_dlg->time();

    // Add time to local storage, will be synced automatically.
    local_records->add_record(active_project, LocalTimeRecords::Entity::Issue, active_issue, time);
}

void Tracker::show_estimate_dialog()
{
    time_dlg->setWindowTitle("Time estimate");
    time_dlg->set_header_text("Update time estimate");
    time_dlg->set_time(active_issue_time_estimate);
    time_dlg->set_button("Update", QIcon::fromTheme("appointment"));

    // Update project info in time entry dialog.
    update_time_dlg_project_info();

    if (time_dlg->exec() != QDialog::Accepted) {
        return;
    }
    set_active_issue_estimate(time_dlg->time());
}

void Tracker::set_active_issue_estimate(long estimate)
{
    blocking_progress->setFixedWidth(width() * 2 / 3);
    blocking_progress->set_message_text("Please wait, updating time estimate...");
    blocking_progress->show();

    glc::AsyncJsonObject *r = client->issues()->set_time_estimate(
        active_project, active_issue, timeutils::format_time_hours(estimate, "0s"));
    // Hide the progress regardless the result.
    connect(r, &glc::AsyncJsonResponse::finished, blocking_progress, &QWidget::hide);
    // Display error if failed.
    connect(r, &glc::AsyncJsonResponse::failed, this, &Tracker::network_failed);
    // Update time estimate, fetch notes if succeed.
    connect(r, &glc::AsyncJsonObject::loaded, issues_model, &AsyncJsonModel::refresh);
}

void Tracker::display_time_records_context_menu(const QPoint &pos)
{
    QTableView * const view = qobject_cast<QTableView *>(sender());
    if (!view) {
        return;
    }

    QModelIndex index = view->indexAt(pos);

    // Clicked on nothing
    if (!index.isValid()) {
        return;
    }
    QMenu *menu = view->property("menu").value<QMenu *>();

    // Disable menu when invoked for estimate action (remote)
    if (view == ui->time_records_remote) {
        const QAbstractItemModel * const model = view->model();
        const QString act =
            model->index(index.row(), AsyncNotesModel::Columns::TIME_ACTION).data().toString();

        menu->actions().at(0)->setEnabled((act == "Added") || (act == "Subtracted"));
    }

    menu->exec(view->viewport()->mapToGlobal(pos));
}

void Tracker::remove_local_time_records()
{
    const QModelIndexList indices = ui->time_records_local->selectionModel()->selectedRows();
    const int num_records = indices.size();
    if (num_records == 0) {
        qWarning() << "remove_local_time_records called with empty selection";
        return;
    }
    // Ask confirmation
    QString title;
    QString message;
    if (num_records > 1) {
        title = "Remove local records?";
        message = QString("Remove %2 local time record (%1)?\nThis operation can't be reversed.")
                  .arg(local_records->data(
                           local_records->index(
                               indices.at(0).row(),
                               LocalTimeRecords::Columns::TIME)
                           ).toString()
                       )
                  .arg(num_records);
    } else {
        title = "Remove local record?";
        message = QString("Remove 1 local time record (%1)?\nThis operation can't be reversed.")
                  .arg(local_records->data(
                           local_records->index(
                               indices.at(0).row(),
                               LocalTimeRecords::Columns::TIME)
                           ).toString()
                       );
    }
    int response = QMessageBox::question(
        this, "Remove local record?",
        QString("Remove 1 local time record (%1)?\nThis operation can't be reversed.")
        .arg(
            local_records->data(
                local_records->index(
                    indices.at(0).row(),
                    LocalTimeRecords::Columns::TIME)
                ).toString()
            ),
        QMessageBox::Yes | QMessageBox::No,
        QMessageBox::No);

    if (response != QMessageBox::Yes) {
        return;
    }
    for (const QModelIndex &index: indices) {
        local_records->remove_record(index.row());
    }
}

void Tracker::revert_remote_time_record()
{
    const QModelIndexList indices = ui->time_records_remote->selectionModel()->selectedRows();
    const int num_records = indices.size();
    if (num_records == 0) {
        qWarning() << "revert_remote_time_record called with empty selection";
        return;
    }
    const QModelIndex &index = indices.at(0);
    const int action_column = AsyncNotesModel::Columns::TIME_ACTION;
    const int time_column = AsyncNotesModel::Columns::TIME_VALUE;
    const QString action =
        notes_model->data(notes_model->index(index.row(), action_column)).toString();
    QString time = notes_model->data(notes_model->index(index.row(), time_column)).toString();
    int response = QMessageBox::question(
        this, "Revert time record?",
        QString("This operation will %1 %2.\nContinue?")
        .arg(action == "Added" ? "subtract" : "add")
        .arg(time),
        QMessageBox::Yes | QMessageBox::No,
        QMessageBox::No);

    if (response != QMessageBox::Yes) {
        return;
    }
    if (action == "Added") {
        time = "-" + time;
    }
    blocking_progress->setFixedWidth(width() * 2 / 3);
    blocking_progress->set_message_text("Please wait, adding time record");
    blocking_progress->show();

    glc::AsyncJsonObject *r = client->issues()->add_spent_time(active_project, active_issue, time);
    // Hide the progress regardless the result.
    connect(r, &glc::AsyncJsonResponse::finished, blocking_progress, &QWidget::hide);
    // Display error if failed.
    connect(r, &glc::AsyncJsonResponse::failed, this, &Tracker::network_failed);
    // Update time estimate, fetch notes if succeed.
    connect(r, &glc::AsyncJsonObject::loaded, issues_model, &AsyncJsonModel::refresh);

}

void Tracker::handle_systray_action(QSystemTrayIcon::ActivationReason reason)
{
    // On mac the activation signal is emitted also for single click activation so need to filter
    if (reason == QSystemTrayIcon::ActivationReason::Context ||
            reason == QSystemTrayIcon::ActivationReason::Trigger)
    {
        return;
    }
    if (!isVisible()) {
        showNormal();
        activateWindow();
        raise();
        setFocus();
    }
}

void Tracker::rebuild_recent_issues_menu()
{
    recent_issues_menu->clear();
    QAbstractItemModel *model = static_cast<QAbstractItemModel *>(sender());
    QFontMetrics fm = time_tracking_menu->fontMetrics();
    const int column = AsyncIssuesModel::Columns::NUM_AND_TITLE;
    const int width = settings->value(keys::max_menu_width, defaults::max_menu_width).toInt();
    const int recent_count = settings->value(
        keys::recent_issues_count, defaults::recent_issues_count).toInt();
    // If recent_count < 0, then no limit, otherwise limit by the setting value.
    const int max_items =
        recent_count > 0 ? std::min(recent_count, model->rowCount()) : model->rowCount();
    for (int i = 0; i < max_items; i++) {
        QVariant issue_title = model->data(model->index(i, column));
        QAction *act = recent_issues_menu->addAction(
            fm.elidedText(issue_title.toString(), Qt::TextElideMode::ElideRight, width));
        act->setCheckable(true);
        // Save original title as data to compare with original, not elided version later.
        act->setData(issue_title);
    }
}

void Tracker::rebuild_recent_projects_menu()
{
    recent_projects_menu->clear();
    QFontMetrics fm = time_tracking_menu->fontMetrics();
    const int column = AsyncProjectsModel::Columns::NAME_WITH_NAMESPACE;
    const int width = settings->value(keys::max_menu_width, defaults::max_menu_width).toInt();
    const int recent_count = settings->value(
        keys::recent_projects_count, defaults::recent_projects_count).toInt();

    // Load saved recent projects list and pre-populate
    const int num_saved = settings->beginReadArray(keys::recent_projects_list);
    QList<int> recent_projects;
    recent_projects.reserve(recent_count + 1);
    for (int i = 0; i < num_saved; ++i) {
        settings->setArrayIndex(i);
        recent_projects.append(settings->value("id").toInt());
    }
    settings->endArray();

    QModelIndex start_idx = projects_model->index(0, AsyncProjectsModel::Columns::ID);
    for (const int project_id: recent_projects) {
        QModelIndexList found = projects_model->match(start_idx, Qt::DisplayRole,
                                                      project_id, 1,
                                                      Qt::MatchExactly);
        if (!found.size()) {
            continue;
        }
        QVariant title = projects_model->index(found.at(0).row(), column).data();
        QAction *act = recent_projects_menu->addAction(
            fm.elidedText(title.toString(), Qt::TextElideMode::ElideRight, width));
        act->setCheckable(true);
        // Save original title as data to compare with original, not elided version later.
        act->setData(title);
    }

}

void Tracker::update_recent_issues_menu_checkstate()
{
    QVariant current_title = ui->issue_mr_combo->currentData(Qt::DisplayRole);
    const QList<QAction *> actions = recent_issues_menu->actions();
    for (QAction *act: actions) {
        act->setChecked(act->data() == current_title);
    }
}

void Tracker::update_recent_projects_menu_checkstate()
{
    QVariant current_title = ui->projects_combo->currentData(Qt::DisplayRole);
    const QList<QAction *> actions = recent_projects_menu->actions();
    for (QAction *act: actions) {
        act->setChecked(act->data() == current_title);
    }
}

void Tracker::handle_recent_issue_activation(QAction *activated)
{
    int index = ui->issue_mr_combo->findData(activated->data(), Qt::DisplayRole);
    if (index < 0) {
        qWarning() << "Not found activated menu item " << activated->data() << " in the combo box";
        return;
    }
    ui->issue_mr_combo->setCurrentIndex(index);
}

void Tracker::handle_recent_project_activation(QAction *activated)
{
    int index = ui->projects_combo->findData(activated->data(), Qt::DisplayRole);
    if (index < 0) {
        qWarning() << "Not found activated menu item " << activated->data() << " in the combo box";
        return;
    }
    ui->projects_combo->setCurrentIndex(index);
}

void Tracker::save_state()
{
    settings->setValue(keys::window_position, pos());
    settings->setValue(keys::window_size, size());

    // The state is saved when the records are toggled off. If they are shown, we need to save it
    // explicitly.
    if (ui->time_records_widget->isVisible()) {
        settings->setValue(keys::time_rec_split, ui->time_records_splitter->saveState());
        settings->setValue(keys::time_rec_height, height());
    }
}

void Tracker::display_overdue_notification(long remaining)
{
    if (remaining > 0) {
        tray_icon->showMessage(
            "Current issue is about to overdue",
            // -1 in max seconds results in seconds never shown.
            QString("Less than %1 of estimated time left.").arg(
                timeutils::format_time(remaining, "0m", -1)),
            QSystemTrayIcon::Warning);
    } else {
        tray_icon->showMessage(
            "Current issue is overdue",
            QString("All estimated time for the issue has been spent."),
            QSystemTrayIcon::Warning);
    }
}

void Tracker::sync_local_time_records()
{
    const QModelIndexList indices = ui->time_records_local->selectionModel()->selectedRows();
    const int num_records = indices.size();
    if (num_records == 0) {
        qWarning() << "remove_local_time_records called with empty selection";
        return;
    }
    // Sync selected records.
    for (const QModelIndex &index: indices) {
        local_records->sync_record(index.row());
    }
}

void Tracker::toggle_time_records(bool show)
{
    // Save time records stat on hide.
    if (!show) {
        settings->setValue(keys::time_rec_split, ui->time_records_splitter->saveState());
        settings->setValue(keys::time_rec_height, height());
    }

    // Toggle controls visibility. Only toggling parent control.
    ui->time_records_widget->setVisible(show);

    // And restore on show.
    if (show) {
        resize(width(), settings->value(keys::time_rec_height).toInt());
        ui->time_records_splitter->restoreState(
            settings->value(keys::time_rec_split).toByteArray());
    }

}

void Tracker::show_settings()
{
    if (!settings_dlg) {
        settings_dlg = new SettingsDialog(this);
    }
    if (settings_dlg->exec() != QDialog::Accepted) {
        return;
    }
    qDebug() << "Updating settings after user edit";
    // Set instance URL and refresh
    const QString instance_url = settings->value(keys::instance_url).toString();
    if (client->instance_url() != instance_url) {
        client->set_instance_url(instance_url);
        projects_model->refresh();
    }
    // Set sync-related timeouts
    local_records->set_auto_sync_period(settings->value(keys::sync_delay).toInt());
    local_records->set_auto_remove_timeout(settings->value(keys::sync_remove_delay).toInt());

    // Set overdue notification timeout
    overdue_notification_time = settings->value(keys::overdue_notif_time).toInt();

    // Reload hotkeys configuration
    load_hotkeys_config();

    // Reflect possible overdue notification change
    const bool use_notif_action = settings->value(keys::overdue_notif_set_estimate,
                                                  defaults::overdue_notif_set_estimate).toBool();
    // Connect with unique connect to account for possibly existing connection
    if (use_notif_action) {
        connect(tray_icon, &QSystemTrayIcon::messageClicked,
                this, &Tracker::suggest_estimate_changing,
                Qt::ConnectionType::UniqueConnection);
    } else {
        disconnect(tray_icon, &QSystemTrayIcon::messageClicked,
                   this, &Tracker::suggest_estimate_changing);
    }

    // Apply cache settings
    // issues_model->set_cache_enabled(
    //             settings->value(keys::enable_cache, defaults::enable_cache).toBool());

}

void Tracker::handle_global_hotkey(size_t id)
{
    switch (id) {
        case Tracker::HotKeys::TOGGLE_TRACKING:
            // Toggle time tracking via the button, but only if enabled.
            // The rest of the actions are attached to the button.
            if (ui->start_stop_tracking->isEnabled()) {
                ui->start_stop_tracking->toggle();
            }
            break;

        case Tracker::HotKeys::TOGGLE_APPWINDOW:
            if (isVisible()) {
                hide();
            } else {
                showNormal();
                activateWindow();
                raise();
            }
            break;

        default:
            qWarning() << "Received activation for unknown global hotkey:" << id;
    }
}

void Tracker::suggest_estimate_changing()
{
    qDebug() << "Suggesting time estimate update";
    QMessageBox msgbox{QMessageBox::Icon::Question,
                       "Update time estimate?",
                       "Would you like to change time estimate?",
                       QMessageBox::StandardButton::No,
                       this};
    QPushButton *open_dlg = msgbox.addButton("Yes, let me choose", QMessageBox::YesRole);
    QPushButton *add1d = msgbox.addButton("Add 1 day", QMessageBox::YesRole);
    QPushButton *add2h = msgbox.addButton("Add 2 hours", QMessageBox::YesRole);
    QPushButton *add1h = msgbox.addButton("Add 1 hour", QMessageBox::YesRole);
    QPushButton *add30m = msgbox.addButton("Add 30 minutes", QMessageBox::YesRole);

    msgbox.setDefaultButton(open_dlg);
    msgbox.setEscapeButton(QMessageBox::StandardButton::No);

    if (msgbox.exec() == QMessageBox::StandardButton::No) {
        return;
    }

    if (msgbox.clickedButton() == open_dlg) {
        show_estimate_dialog();
    } else if (msgbox.clickedButton() == add30m) {
        set_active_issue_estimate(active_issue_time_estimate + 30 * timeutils::seconds_per_min);
    } else if (msgbox.clickedButton() == add1h) {
        set_active_issue_estimate(active_issue_time_estimate + 1 * timeutils::seconds_per_hour);
    } else if (msgbox.clickedButton() == add2h) {
        set_active_issue_estimate(active_issue_time_estimate + 2 * timeutils::seconds_per_hour);
    } else if (msgbox.clickedButton() == add1d) {
        set_active_issue_estimate(active_issue_time_estimate + 1 * timeutils::seconds_per_day);
    }
}

void Tracker::hide_to_tray()
{
    hide();
#ifdef Q_OS_MAC
    ProcessSerialNumber psn = {0, kCurrentProcess};
    TransformProcessType(&psn, kProcessTransformToUIElementApplication);
#endif
}

void Tracker::restore_from_tray()
{
    show();
#ifdef Q_OS_MAC
    ProcessSerialNumber psn = {0, kCurrentProcess};
    TransformProcessType(&psn, kProcessTransformToForegroundApplication);
#endif
}

void Tracker::setup_async_combo_ui(QComboBox *combo, QAbstractButton *refresh_button,
                                   AsyncJsonModel *model)
{
    // Setup spinner animation on combo box' line edit.
    QWidgetAction *wa = new QWidgetAction(this);
    QLabel *label = new QLabel(this);
    wa->setDefaultWidget(label);
    spinner_animation->animate(label);
    combo->lineEdit()->addAction(wa, QLineEdit::LeadingPosition);

    // Connect UI to follow model state.
    connect(model, &AsyncJsonModel::updating, label, &QLabel::setVisible);
    connect(model, &AsyncJsonModel::updating, refresh_button, &QPushButton::setDisabled);
    // Single-time connection to trigger signal on first update
    QMetaObject::Connection *conn = new QMetaObject::Connection();
    *conn = connect(model, &AsyncJsonModel::update_finished, combo, [conn, combo]() {
                        QObject::disconnect(*conn);
                        delete conn;
                        emit combo->currentIndexChanged(-1);
                    });

    // Make button initiate model refresh.
    connect(refresh_button, &QPushButton::clicked, model, &AsyncJsonModel::refresh);

    // Set current state to match model state.
    label->setVisible(model->is_updating());
    refresh_button->setDisabled(model->is_updating());
}

void Tracker::hideEvent(QHideEvent *event)
{
    settings->setValue(keys::window_position, pos());
    if (event->spontaneous()) {
        return;
    }
    ui->show_main_window_act->setVisible(true);
    ui->hide_main_window_act->setVisible(false);
}

void Tracker::showEvent(QShowEvent * /*event*/)
{
    ui->show_main_window_act->setVisible(false);
    ui->hide_main_window_act->setVisible(true);
    move(settings->value(keys::window_position).toPoint());
}

bool Tracker::eventFilter(QObject *watched, QEvent *event)
{
    if ((watched == ui->issue_mr_combo) ||
        (watched == ui->projects_combo))
    {
        QLineEdit *edit = static_cast<QComboBox *>(watched)->lineEdit();
        if (event->type() == QEvent::FocusIn) {
            // Need to filter the first mouse click event after focus to keep text selected
            // if focus obtained with the mouse.
            if (static_cast<QFocusEvent *>(event)->reason() == Qt::FocusReason::MouseFocusReason) {
                edit->setProperty("filter-click", true);
            }
            edit->end(false);
            edit->home(true);
        }
    }

    if ((watched == ui->issue_mr_combo->lineEdit()) ||
        (watched == ui->projects_combo->lineEdit()))
    {
        QLineEdit *edit = static_cast<QLineEdit *>(watched);
        if (event->type() == QEvent::MouseButtonPress) {
            // Set property to false after filtering
            if (edit->property("filter-click").toBool()) {
                edit->setProperty("filter-click", false);
                return true;
            }
        }
    }
    return false;
}

void Tracker::closeEvent(QCloseEvent *event)
{
    // Hide window and ignore event.
    hide_to_tray();
    event->ignore();
}


void Tracker::restore_state()
{
    active_project = settings->value(keys::active_project, 0).toInt();
    active_issue = settings->value(QString("%1/%2")
                                   .arg(keys::last_issues_group)
                                   .arg(active_project), 0).toInt();

    qDebug() << "Loaded active project and issue: " << active_project << ":" << active_issue;

    // Only restore window position and size if there is a value in settings
    if (settings->value(keys::window_position).isValid()) {
        move(settings->value(keys::window_position).toPoint());
    }
    if (settings->value(keys::window_size).isValid()) {
        resize(settings->value(keys::window_size).toSize());
    }

    overdue_notification_time =
        settings->value(keys::overdue_notif_time, defaults::overdue_notif_time).toInt();

    // issues_model->set_cache_enabled(
    //             settings->value(keys::enable_cache, defaults::enable_cache).toBool());
}


void Tracker::setup_projects_ui()
{
    QSortFilterProxyModel *proxy_model = new QSortFilterProxyModel(projects_model);
    proxy_model->setSourceModel(projects_model);
    proxy_model->setDynamicSortFilter(true);
    proxy_model->sort(AsyncProjectsModel::Columns::NAME_WITH_NAMESPACE);
    ui->projects_combo->setModel(proxy_model);

    // Setup completer.
    ui->projects_combo->setModelColumn(AsyncProjectsModel::Columns::NAME_WITH_NAMESPACE);
    ui->projects_combo->completer()->setCompletionMode(QCompleter::PopupCompletion);
    ui->projects_combo->completer()->setFilterMode(Qt::MatchContains);
    ui->projects_combo->completer()->setModelSorting(
        QCompleter::ModelSorting::CaseInsensitivelySortedModel);

    // Setup UI of the projects combo box.
    setup_async_combo_ui(ui->projects_combo, ui->refresh_projects, projects_model);
    ui->projects_combo->lineEdit()->setPlaceholderText("Type to filter");
    connect(projects_model, &AsyncJsonModel::updating, this, &Tracker::update_project_selection_ui);

    // Setup event filters on combo & combo line edits to select all text on focus
    ui->projects_combo->installEventFilter(this);
    ui->projects_combo->lineEdit()->installEventFilter(this);

    // Initial UI update.
    update_project_selection_ui();
}


void Tracker::setup_issues_ui()
{
    // FIXME: Forcibly disabled cache
    issues_model->set_cache_enabled(false);

    // Setup proxy model to sort issues
    QSortFilterProxyModel *proxy_model = new QSortFilterProxyModel(issues_model);
    proxy_model->setSourceModel(issues_model);
    proxy_model->setDynamicSortFilter(true);
    proxy_model->sort(AsyncIssuesModel::Columns::IID);

    // Setup completer.
    QCompleter *completer = ui->issue_mr_combo->completer();
    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setFilterMode(Qt::MatchContains);
    completer->setModelSorting(QCompleter::ModelSorting::CaseInsensitivelySortedModel);
    PopupView *popup = new PopupView();
    completer->setPopup(popup);
    popup->setModelColumn(AsyncIssuesModel::Columns::NUM_AND_TITLE);

    ui->issue_mr_combo->setModel(proxy_model);
    ui->issue_mr_combo->setModelColumn(AsyncIssuesModel::Columns::NUM_AND_TITLE);

    // Setup UI of the issues combo box.
    setup_async_combo_ui(ui->issue_mr_combo, ui->refresh_issues, issues_model);
    ui->issue_mr_combo->lineEdit()->setPlaceholderText("Type to filter");
    connect(issues_model, &AsyncJsonModel::updating, this, &Tracker::update_issue_mr_selection_ui);

    // Setup event filters on combo & combo line edits to select all text on focus
    ui->issue_mr_combo->installEventFilter(this);
    ui->issue_mr_combo->lineEdit()->installEventFilter(this);

    // Set issue filter & event actions
    issues_model->filter().state("opened");
    connect(issues_model, &AsyncIssuesModel::failed, this, &Tracker::network_failed);
    connect(issues_model, &AsyncIssuesModel::update_finished, this, [this]() {
                AsyncIssuesModel *model = static_cast<AsyncIssuesModel*>(sender());
                bool has_results = model->rowCount() > 0;
                ui->issue_mr_combo->setEnabled(has_results);
                recent_issues_menu->menuAction()->setVisible(has_results);
                if (!has_results) {
                    ui->issue_mr_combo->setCurrentText("Project has no issues");
                }
            });


    // Initial UI update.
    update_issue_mr_selection_ui();
}

void Tracker::setup_time_records_ui()
{
    notes_model->set_column_title(AsyncNotesModel::Columns::TIME_ACTION, "Action");
    notes_model->set_column_title(AsyncNotesModel::Columns::TIME_VALUE, "Time");
    notes_model->set_column_title(AsyncNotesModel::Columns::CREATED_AT, "When");
    QSortFilterProxyModel *proxy_model = new QSortFilterProxyModel(notes_model);
    proxy_model->setSourceModel(notes_model);
    proxy_model->setFilterKeyColumn(AsyncNotesModel::Columns::TIME_ACTION);
    proxy_model->setFilterRegExp(".");
    ui->time_records_remote->setModel(proxy_model);
    // Hide all but the last 3 columns
    for (int i = 0, len = notes_model->columnCount() - 3; i < len; i++) {
        ui->time_records_remote->hideColumn(i);
    }
    // Reorder columns
    QHeaderView *header = ui->time_records_remote->horizontalHeader();
    header->moveSection(header->visualIndex(AsyncNotesModel::Columns::TIME_ACTION), 0);
    header->moveSection(header->visualIndex(AsyncNotesModel::Columns::TIME_VALUE), 1);

    // auto resize columns
    ui->time_records_remote->horizontalHeader()->setSectionResizeMode(
        QHeaderView::ResizeMode::Stretch);

    // Local time records
    local_records->set_gitlab_client(client);
    local_records->set_projects_model(projects_model);
    ui->time_records_local->setModel(local_records);
    // auto resize columns
    ui->time_records_local->horizontalHeader()->setSectionResizeMode(
        QHeaderView::ResizeMode::Stretch);
    // Hide separate columns: project, id, entity
    ui->time_records_local->hideColumn(LocalTimeRecords::Columns::ID);
    ui->time_records_local->hideColumn(LocalTimeRecords::Columns::ENTITY);
    ui->time_records_local->hideColumn(LocalTimeRecords::Columns::PROJECT);

    // Setup show / hide depending on if we have local records
    connect(local_records, &LocalTimeRecords::rowsInserted,
            this, &Tracker::update_local_time_record_visibility);
    connect(local_records, &LocalTimeRecords::rowsRemoved,
            this, &Tracker::update_local_time_record_visibility);
    // Update displayed time on sync
    connect(local_records, &LocalTimeRecords::synced, this, &Tracker::local_time_synced);
    // Set initial state
    update_local_time_record_visibility();

    // Hide time records initially.
    ui->time_records_widget->hide();

    // Local records context menu.
    QMenu *menu = new QMenu(this);
    menu->addAction(ui->sync_now_act);
    menu->addAction(ui->remove_time_record_act);
    ui->time_records_local->setProperty("menu", QVariant::fromValue(menu));

    // Pause TTS timer while menu is open
    connect(menu, &QMenu::aboutToShow, local_records, &LocalTimeRecords::stop_tts_timer);
    connect(menu, &QMenu::aboutToHide, local_records, &LocalTimeRecords::start_tts_time);

    // Remote records context menu.
    menu = new QMenu(this);
    menu->addAction(ui->revert_time_record_act);
    ui->time_records_remote->setProperty("menu", QVariant::fromValue(menu));

}

void Tracker::setup_gitlab_connection()
{
    // Fetch projects if we have token, request token otherwise
    QString token = settings->value(keys::auth_token).toString();
    connect(client, &glc::Client::auth_failed,
            this, [this](const QString &err, QString desc, const QUrl &) {
                QMessageBox::critical(
                    this, "GitLab Tracker",
                    QString("<p><b>Failed to authorize application with GitLab.</b></p>"
                            "<p>Error was: <code>%1</code></p><p>%2</p>").arg(
                        err, desc.replace('+', ' ')));
            });
    connect(client, &glc::Client::auth_succeed, this, [this]() {
                settings->setValue(keys::auth_token, client->token());
                fetch_projects();
            });
    // Set instance URL from settings (empty URL is OK).
    client->set_instance_url(settings->value(keys::instance_url).toString());

    if (!token.isEmpty()) {
        client->set_token(token);
        fetch_projects();
    } else {
        request_authorization();
    }
}

void Tracker::setup_time_tracking_ui()
{
    // Hide remaining label.
    ui->remaining_label->hide();

    // Set pixmap based on the time icon (emoji doesn't work).
    // TODO: make decision portable. Check label size on Linux, when emoji not found.
#ifndef Q_OS_MAC
    ui->time_icon->setPixmap(QIcon::fromTheme("time").pixmap(22, 22));
#endif

    // Will be shown when time tracking starts.
    ui->now_tracking_widget->hide();

    // Setup timers.
    switch_update_resolution_timer.setSingleShot(true);
    // Make sure it switched after time updated by the update timer, so add a second.
    switch_update_resolution_timer.setInterval(max_seconds * 1000 + 1000);
    connect(&switch_update_resolution_timer, &QTimer::timeout, this, [this]() {
                update_tracking_ui_timer.setInterval(timeutils::seconds_per_min * 1000);
            });
    connect(&update_tracking_ui_timer, &QTimer::timeout, this, &Tracker::update_time_tracking_ui);
}

void Tracker::setup_tray_menu()
{
    // System tray icon & actions
    tray_icon->show();
    connect(tray_icon, &QSystemTrayIcon::activated, this, &Tracker::handle_systray_action);
    if (settings->value(keys::overdue_notif_set_estimate,
                        defaults::overdue_notif_set_estimate).toBool())
    {
        connect(tray_icon, &QSystemTrayIcon::messageClicked,
                this, &Tracker::suggest_estimate_changing);
    }

    QMenu *tray_menu = new QMenu("GitLab Tracker", this);

    // Show / hide window
    tray_menu->addAction(ui->show_main_window_act);
    tray_menu->addAction(ui->hide_main_window_act);

    tray_menu->addSeparator();

    // Project information
    recent_projects_menu = tray_menu->addMenu("Choose project");

    // Issue information
    recent_issues_menu = tray_menu->addMenu("Choose issue");
    recent_issues_menu->menuAction()->setVisible(false);

    // Now tracking menu: spent / estimate & submenu
    time_tracking_menu = tray_menu->addMenu("Now tracking");
    time_tracking_menu->menuAction()->setVisible(false);
    time_tracking_menu->addAction(ui->set_time_estimate_act);
    time_tracking_menu->addAction(ui->add_manual_time_act);

    // Track menu status to disable title updates while shown
    connect(time_tracking_menu, &QMenu::aboutToShow,
            this, [this]() { time_tracking_menu_shown = true; });
    // When hidden, also update tracking UI to immediately show current time
    connect(time_tracking_menu, &QMenu::aboutToHide,
            this, [this]() { time_tracking_menu_shown = false; update_time_tracking_ui(); });

    // Start / stop tracking
    tray_menu->addAction(ui->start_tracking_act);
    tray_menu->addAction(ui->stop_tracking_act);
    tray_menu->addAction(ui->cancel_tracking_act);

    tray_menu->addSeparator();

    // Settings
    tray_menu->addAction("Settings", this, &Tracker::show_settings);
    // About application
    tray_menu->addAction(
        "About GitLab Tracker...", this, [=]() { (new AboutDialog(this))->show(); });

    tray_menu->addSeparator();

    // quit action
    tray_menu->addAction(ui->quit_application_act);
    connect(ui->quit_application_act, &QAction::triggered, this, &QApplication::quit);

    tray_icon->setContextMenu(tray_menu);

    // Menu item with a list of issues:
    // 1. Proxy model for recent issues
    recent_issues_model = new QSortFilterProxyModel(this);
    recent_issues_model->setSourceModel(issues_model);
    recent_issues_model->setDynamicSortFilter(true);
    recent_issues_model->sort(AsyncIssuesModel::Columns::UPDATED_AT, Qt::DescendingOrder);
    // 2. Connect model reset signal to rebuild submenu
    connect(recent_issues_model, &QSortFilterProxyModel::modelReset,
            this, &Tracker::rebuild_recent_issues_menu);
    // 3. Switch issue on menu selection
    connect(recent_issues_menu, &QMenu::triggered,
            this, &Tracker::handle_recent_issue_activation);

    // Menu item with a list of projects:
    // Switch project on menu selection
    connect(recent_projects_menu, &QMenu::triggered,
            this, &Tracker::handle_recent_project_activation);
}

