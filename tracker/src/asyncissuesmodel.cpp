#include <cassert>

#include <QStandardPaths>

#include "timeutils.h"

#include "asyncissuesmodel.h"
#include "gitlabclient/issues.h"

const QStringList AsyncIssuesModel::json_fields{
    "id", "iid", "title", "state", "description", "project_id"
};


AsyncIssuesModel::AsyncIssuesModel(gitlabclient::Client *client, QObject *parent):
    AsyncJsonModel(AsyncIssuesModel::json_fields, nullptr, parent),
    issues(new gitlabclient::Issues(client))
{

    assert(client && "GitlabClient instance must be not null");

    const int updated_at_column = add_computed_column(
        "updated-at",
        [](const QJsonObject &issue, int role) -> QVariant {
            if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
                return issue["updated_at"].toVariant();
            }
            if (role == Roles::DATETIME) {
                return QDateTime::fromString(issue["updated_at"].toString(),
                                             Qt::DateFormat::ISODateWithMs).toLocalTime();
            }
            return QVariant();
        });

    const int issue_id_title_column = add_computed_column(
        "id-title",
        [](const QJsonObject &issue, int role) -> QVariant {
            if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
                return QString("#%1: %2").arg(issue["iid"].toInt()).arg(issue["title"].toString());
            }
            return QVariant();
        });

    const int time_spent_column = add_computed_column(
        "time-spent",
        [](const QJsonObject &issue, int role) -> QVariant {
            int spent = issue["time_stats"].toObject()["total_time_spent"].toInt();
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                return timeutils::format_time(spent, "No spent time reported");
            }
            if (role == Roles::NUMERIC || role == Qt::EditRole) {
                return spent;
            }
            return QVariant();
        });

    const int time_estimate_column = add_computed_column(
        "time-estimate",
        [](const QJsonObject &issue, int role) -> QVariant {
            int estimate = issue["time_stats"].toObject()["time_estimate"].toInt();
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                return timeutils::format_time(estimate, "No estimate set");
            }
            if (role == Roles::NUMERIC || role == Qt::EditRole) {
                return estimate;
            }
            return QVariant();
        });

    assert(column_index("id") == Columns::ID);
    assert(column_index("iid") == Columns::IID);
    assert(column_index("title") == Columns::TITLE);
    assert(column_index("state") == Columns::STATE);
    assert(column_index("description") == Columns::DESCRIPTION);
    assert(column_index("project_id") == Columns::PROJECT_ID);
    assert(updated_at_column == Columns::UPDATED_AT);
    assert(issue_id_title_column == Columns::NUM_AND_TITLE);
    assert(time_spent_column == Columns::TIME_SPENT);
    assert(time_estimate_column == Columns::TIME_ESTIMATE);

    cache_dir.setPath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    if (!cache_dir.exists()) {
        cache_dir.mkpath(".");
    }
}

void AsyncIssuesModel::load_all_issues()
{
    if (use_cache) {
        const QString fname = cache_dir.filePath("all-issues.json");
        issues->updated_after(preload_cache(fname));
    }
    set_objects_list(issues->list_issues());
}

void AsyncIssuesModel::load_project_issues(long id)
{
    if (use_cache) {
        const QString fname = cache_dir.filePath(QString("project-%2-issues.json").arg(id));
        issues->updated_after(preload_cache(fname));
    }
    set_objects_list(issues->list_project_issues(id));
}

void AsyncIssuesModel::load_group_issues(long id)
{
    if (use_cache) {
        const QString fname = cache_dir.filePath(QString("group-%2-issues.json").arg(id));
        issues->updated_after(preload_cache(fname));
    }
    set_objects_list(issues->list_group_issues(id));
}

void AsyncIssuesModel::set_cache_enabled(bool enable)
{
    use_cache = enable;
    if (use_cache) {
        set_cache_key("id");
    } else {
        set_cache_key(QString{});
    }
}

