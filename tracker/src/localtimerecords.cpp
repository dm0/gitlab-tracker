#include <QMetaEnum>

#include "localtimerecords.h"
#include "timeutils.h"

using namespace std::placeholders;
namespace glc = gitlabclient;

LocalTimeRecords::LocalTimeRecords(QObject *parent):
    QAbstractTableModel(parent),
    column_names{{Columns::ID, "Id"},
                 {Columns::TIME, "Time"},
                 {Columns::ENTITY, "Entity"},
                 {Columns::STATUS, "Status"},
                 {Columns::PROJECT_ENTITY, "Target"},
                 {Columns::PROJECT, "Project"}},
    auto_sync_timer(this)
{
    // Must be 1 sec interval timer for the TTS logic to work.
    auto_sync_timer.setInterval(1000);
    connect(&auto_sync_timer, &QTimer::timeout, this, &LocalTimeRecords::sync);
}

QVariant LocalTimeRecords::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Orientation::Horizontal || role != Qt::DisplayRole) {
        return QAbstractTableModel::headerData(section, orientation, role);
    }
    if (section < 0 || section >= Columns::NUM_COLUMNS) {
        return QVariant();
    }
    return column_names.value(section);
}

int LocalTimeRecords::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return records.size();
}

int LocalTimeRecords::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return Columns::NUM_COLUMNS;
}

QVariant LocalTimeRecords::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    const int column = index.column();
    const int row = index.row();
    if (column < 0 || column >= Columns::NUM_COLUMNS) {
        return QVariant();
    }
    if (row < 0 || row >= records.size()) {
        return QVariant();
    }

    const TimeRecord &record = records.at(row);

    switch (column) {
        case Columns::ID:
            if (role == Roles::ID) {
                return QVariant::fromValue<long long>(record.entity_id);
            }
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                QString result;
                switch (record.entity) {
                    case Entity::Issue:
                        result = "#%1";
                        break;

                    case Entity::MergeRequest:
                        result = "!%1";
                        break;
                }
                return result.arg(record.entity_id);
            }
            break;

        case Columns::PROJECT:
            if (role == Roles::ID) {
                return QVariant::fromValue<long long>(record.project_id);
            }
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                // If projects model is set, query it for the project name.
                if (projects_model) {
                    QModelIndex start_idx =
                        projects_model->index(0, AsyncProjectsModel::Columns::ID);
                    QModelIndexList found =
                        projects_model->match(start_idx, Qt::DisplayRole,
                                              QVariant::fromValue<long long>(record.project_id), 1,
                                              Qt::MatchExactly);
                    if (found.size()) {
                        return projects_model->index(
                            found.at(0).row(), AsyncProjectsModel::Columns::NAME).data();
                    }
                }
                return QString::number(record.project_id);
            }
            break;

        case Columns::TIME:
            if (role == Roles::TIME_SECONDS) {
                return QVariant::fromValue<long long>(record.tracked_time_sec);
            }
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                return timeutils::format_time(record.tracked_time_sec, "0s");
            }
            break;

        case Columns::ENTITY:
            if (role == Roles::ENTITY) {
                return static_cast<int>(record.entity);
            }
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                switch (record.entity) {
                    case Entity::Issue:
                        return "Issue";

                    case Entity::MergeRequest:
                        return "MergeRequest";
                }
            }
            break;

        case Columns::STATUS:
            if (role == Roles::RECORD_STATUS) {
                return static_cast<int>(record.status);
            }
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                switch (record.status) {
                    case RecordStatus::Local:
                        if (record.tts == 0) {
                            return "Sending";
                        }
                        return QString("Local (%1)").arg(record.tts);

                    case RecordStatus::Sent:
                        return "Sent";

                    case RecordStatus::Synced:
                        return "Synced";

                    case RecordStatus::Error:
                        return "Error";
                }
            }
            break;

        case Columns::PROJECT_ENTITY:
            if (role == Qt::DisplayRole || role == Qt::ToolTipRole) {
                const QString project = this->index(row, Columns::PROJECT).data().toString();
                const QString id = this->index(row, Columns::ID).data().toString();
                return QString("%1 %2").arg(project).arg(id);
            }
            break;

        default: /* Should never happen */
            return QVariant();
    }
    return QVariant();
}

void LocalTimeRecords::set_auto_remove_timeout(int seconds)
{
    auto_remove_timeout_sec = seconds;
}

void LocalTimeRecords::set_auto_sync_period(int seconds)
{
    auto_sync_period_sec = seconds;
}

int LocalTimeRecords::get_local_tracked_time(long project_id, LocalTimeRecords::Entity entity,
                                             long entity_id)
{
    int total = 0;
    for (const TimeRecord &record: qAsConst(records)) {
        // Skip not target records
        if (record.project_id != project_id || record.entity != entity ||
            record.entity_id != entity_id)
        {
            continue;
        }
        // Skip not local records
        if (record.status == RecordStatus::Synced) {
            continue;
        }
        total += record.tracked_time_sec;
    }
    return total;
}

void LocalTimeRecords::add_record(long project, Entity type, long id, long time_seconds)
{
    const int new_index = records.size();
    beginInsertRows(QModelIndex{}, new_index, new_index);
    records.append({RecordStatus::Local, auto_sync_period_sec,
                    time_seconds, type, id, project});
    endInsertRows();
    // The first record added, start auto sync timer if not disabled
    if (!new_index && auto_sync_period_sec > 0) {
        auto_sync_timer.start();
    }
}

void LocalTimeRecords::sync()
{
    if (!client) {
        qWarning() << "GitLab client is not set, can't sync!";
        return;
    }
    unsigned int records_updated = 0;
    for (int row = 0, len = records.size(); row < len; ++row) {
        TimeRecord &record = records[row];
        // Skip already syncing or synced records
        if (record.status != RecordStatus::Local) {
            continue;
        }
        ++records_updated;
        // Skip records with TTS above zero after decrement
        if ((auto_sync_period_sec > 0) && (record.tts > 1)) {
            --record.tts;
            continue;
        }
        glc::AsyncJsonObject *response;
        switch (record.entity) {
            case Entity::Issue:
                response = client->issues()->add_spent_time(
                    QString::number(record.project_id), record.entity_id,
                    timeutils::format_time(record.tracked_time_sec));
                break;

            case Entity::MergeRequest:
                response = client->merge_requests()->add_spent_time(
                    QString::number(record.project_id), record.entity_id,
                    timeutils::format_time(record.tracked_time_sec));
                break;
        }
        record.status = RecordStatus::Sent;

        connect(response, &glc::AsyncJsonObject::loaded,
                this, std::bind(&LocalTimeRecords::update_synced_record, this, row));

        connect(response, &glc::AsyncJsonObject::failed,
                this, std::bind(&LocalTimeRecords::update_failed_record, this, row, _1));

    }
    // Updated whole column if any record was updated.
    if (records_updated) {
        emit dataChanged(index(0, Columns::STATUS), index(records.size() - 1, Columns::STATUS));
    }
}


void LocalTimeRecords::remove_record(int record_index)
{
    beginRemoveRows(QModelIndex{}, record_index, record_index);
    records.remove(record_index);
    endRemoveRows();
    // Stop timer if all records are removed.
    if (records.size() == 0) {
        auto_sync_timer.stop();
    }
}

void LocalTimeRecords::remove_record(QPersistentModelIndex index)
{
    if (!index.isValid()) {
        return;
    }
    remove_record(index.row());
}

void LocalTimeRecords::sync_record(int record_index)
{
    const int rsize = records.size();
    if (record_index < 0 || record_index >= rsize) {
        qWarning() << "sync_record called with invalid record index:" << record_index
                   << "(total:" << rsize << ")";
        return;
    }
    TimeRecord &record = records[record_index];
    if (record.status != RecordStatus::Local) {
        qWarning() << "sync_record called for non-local record";
        return;
    }
    // Sync on next tick.
    record.tts = 0;
}

void LocalTimeRecords::toggle_tts_timer(bool run)
{
    if (!run) {
        auto_sync_timer.stop();
    } else if (records.size() > 0) {
        // Start timer only if we have records.
        auto_sync_timer.start();
    }
}

void LocalTimeRecords::update_synced_record(int row_index)
{
    glc::AsyncJsonObject *response = qobject_cast<glc::AsyncJsonObject *>(sender());
    if (!response) {
        return;
    }
    qDebug() << response->response_time();
    TimeRecord &record = records[row_index];
    record.status = RecordStatus::Synced;
    QPersistentModelIndex idx{index(row_index, Columns::STATUS)};
    emit dataChanged(idx, idx);
    // Remove after timeout
    QTimer::singleShot(auto_remove_timeout_sec * 1000, this, [this, idx]() { remove_record(idx); });

    int spent = response->json().object()["total_time_spent"].toInt();
    emit synced(record.project_id, record.entity, record.entity_id, spent,
                response->response_time());

}

void LocalTimeRecords::update_failed_record(int row_index, QNetworkReply::NetworkError error)
{
    glc::AsyncJsonObject *response = qobject_cast<glc::AsyncJsonObject *>(sender());
    QString text = QString("Network request failed with error: %1")
                   .arg(QMetaEnum::fromType<QNetworkReply::NetworkError>().
                        valueToKey(error));
    text += "\nResponse:" + response->json().toJson();
    qDebug() << text;
    records[row_index].status = RecordStatus::Error;
    QModelIndex idx = index(row_index, Columns::STATUS);
    emit dataChanged(idx, idx);
}

