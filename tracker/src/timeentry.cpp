#include <QDebug>

#include "timeutils.h"

#include "timeentry.h"
#include "ui_timeentry.h"

TimeEntry::TimeEntry(QWidget *parent, QString settings_group):
    QDialog(parent),
    ui(new Ui::TimeEntry),
    settings_menu(new QMenu(this)),
    settings_group(settings_group)
{
    ui->setupUi(this);

    // Enter default settings group.
    settings.beginGroup(settings_group);

    // Reset time to zero.
    set_time(0);

    // Setup settings menu
    settings_menu->addAction(ui->toggle_free_text_input);
    settings_menu->addSection("Visible controls");

    QActionGroup *agrp = new QActionGroup(this);
    agrp->setExclusive(false);

    agrp->addAction(ui->toggle_months);
    agrp->addAction(ui->toggle_days);
    agrp->addAction(ui->toggle_hours);
    agrp->addAction(ui->toggle_minutes);
    agrp->addAction(ui->toggle_seconds);

    settings_menu->addActions({ui->toggle_months, ui->toggle_days, ui->toggle_hours,
                               ui->toggle_minutes, ui->toggle_seconds});

    ui->menu_button->setMenu(settings_menu);

    // Disable control-related actions on free text toggle
    connect(ui->toggle_free_text_input, &QAction::toggled,
            agrp, &QActionGroup::setDisabled);
    // Update toggle actions availability to prevent hiding the last control.
    connect(agrp, &QActionGroup::triggered, this, &TimeEntry::update_toggle_actions);

    // List of time controls
    time_controls.append({
        {ui->months, ui->toggle_months},
        {ui->days, ui->toggle_days},
        {ui->hours, ui->toggle_hours},
        {ui->minutes, ui->toggle_minutes},
        {ui->seconds, ui->toggle_seconds}
    });
    // Set dynamic property "index"
    for (int i = 0, len = time_controls.length(); i < len; ++i) {
        time_controls.at(i).first->setProperty("index", i);
    }

    // Restore UI state.
    restore_ui_state();
}

TimeEntry::~TimeEntry()
{
    save_ui_state();
    delete ui;
}

void TimeEntry::save_ui_state()
{
    settings.beginGroup("visibility");
    settings.setValue("months", ui->toggle_months->isChecked());
    settings.setValue("days", ui->toggle_days->isChecked());
    settings.setValue("hours", ui->toggle_hours->isChecked());
    settings.setValue("minutes", ui->toggle_minutes->isChecked());
    settings.setValue("seconds", ui->toggle_seconds->isChecked());
    settings.endGroup();
    settings.setValue("free-mode", ui->toggle_free_text_input->isChecked());
}

void TimeEntry::restore_ui_state()
{
    settings.beginGroup("visibility");
    ui->toggle_months->setChecked(settings.value("months", true).toBool());
    ui->toggle_days->setChecked(settings.value("days", true).toBool());
    ui->toggle_hours->setChecked(settings.value("hours", true).toBool());
    ui->toggle_minutes->setChecked(settings.value("minutes", true).toBool());
    ui->toggle_seconds->setChecked(settings.value("seconds", true).toBool());
    settings.endGroup();
    ui->toggle_free_text_input->setChecked(settings.value("free-mode", false).toBool());
}

void TimeEntry::update_toggle_actions()
{
    int num_checked = 0;
    QAction *last = nullptr;
    for (QAction *act: {ui->toggle_months, ui->toggle_days, ui->toggle_hours,
                        ui->toggle_minutes, ui->toggle_seconds})
    {
        if (!act->isEnabled()) {
            act->setEnabled(true);
        }
        if (!act->isChecked()) {
            continue;
        }
        last = act;
        ++num_checked;
    }
    if (num_checked == 1 && last != nullptr) {
        last->setEnabled(false);
    }
}

void TimeEntry::parse_time_string(const QString &value)
{
    total_time = timeutils::parse_time_string(value, timeutils::seconds_per_hour);
    update_current_time_string();
}

void TimeEntry::update_time_from_controls()
{
    using namespace timeutils;
    total_time = 0;
    total_time += ui->months->value() * seconds_per_month;
    total_time += ui->days->value() * seconds_per_day;
    total_time += ui->hours->value() * seconds_per_hour;
    total_time += ui->minutes->value() * seconds_per_min;
    total_time += ui->seconds->value();
    update_current_time_string();
}

void TimeEntry::toggle_free_text(bool free_mode)
{
    // Switch page on free text toggle
    ui->stacked_widget->setCurrentIndex(free_mode);

    // Switched to free form input
    if (free_mode) {
        ui->freetext_input->setText(timeutils::format_time(total_time, "0s"));
    } else {
        // If entered string is invalid, revert to the values in the controls.
        if (total_time > 0) {
            set_time(total_time);
        } else {
            update_time_from_controls();
        }
    }
}

void TimeEntry::set_time(long seconds)
{
    using namespace timeutils;
    int months = static_cast<int>(seconds / seconds_per_month);
    int days = static_cast<int>(seconds % seconds_per_week / seconds_per_day);
    int hours = static_cast<int>(seconds % seconds_per_day / seconds_per_hour);
    int minutes = static_cast<int>(seconds % seconds_per_hour / seconds_per_min);
    int secs = static_cast<int>(seconds % seconds_per_min);
    ui->months->blockSignals(true);
    ui->months->setValue(months);
    ui->months->blockSignals(false);

    ui->days->blockSignals(true);
    ui->days->setValue(days);
    ui->days->blockSignals(false);

    ui->hours->blockSignals(true);
    ui->hours->setValue(hours);
    ui->hours->blockSignals(false);

    ui->minutes->blockSignals(true);
    ui->minutes->setValue(minutes);
    ui->minutes->blockSignals(false);

    ui->seconds->blockSignals(true);
    ui->seconds->setValue(secs);
    ui->seconds->blockSignals(false);

    total_time = seconds;
    update_current_time_string();
    ui->freetext_input->setText(timeutils::format_time(total_time, "0s"));
}

void TimeEntry::set_header_text(const QString &text)
{
    ui->title->setText(text);
}

void TimeEntry::set_button(const QString &text, const QIcon &icon)
{
    ui->submit->setText(text);
    ui->submit->setIcon(icon);
}

void TimeEntry::set_project_info(const QString &title, const QString &tooltip)
{
    ui->project_info->setHidden(title.isEmpty());
    ui->project_info->setText(title);
    ui->project_info->setToolTip(tooltip);
}

void TimeEntry::handle_wrap(int direction)
{
    QSpinBox *sbox = qobject_cast<QSpinBox *>(sender());
    if (!sbox) {
        qCritical() << "Failed to cast sender to QSpinBox";
        return;
    }
    int index = sbox->property("index").toInt();
    if (index) {
        QSpinBox * const ctrl = time_controls.at(index - 1).first;
        ctrl->stepBy(direction);
        // Show next in the chain if it is not visible
        if (!ctrl->isVisible()) {
            time_controls.at(index - 1).second->setChecked(true);
        }
    }
}

void TimeEntry::update_current_time_string()
{
    if (total_time < 0) {
        ui->parsed_time->setText("Invalid time");
    } else {
        ui->parsed_time->setText(timeutils::format_time(total_time, "0s"));
    }
}
