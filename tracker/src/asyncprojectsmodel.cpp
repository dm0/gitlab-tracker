#include "asyncprojectsmodel.h"

#include <cassert>

const QStringList AsyncProjectsModel::json_fields{
    "id", "name", "name_with_namespace", "path_with_namespace", "avatar_url", "description"
};

AsyncProjectsModel::AsyncProjectsModel(gitlabclient::AsyncJsonObjList *objlist, QObject *parent):
    AsyncJsonModel(AsyncProjectsModel::json_fields, objlist, parent)
{    
    const int last_activity_column = add_computed_column(
        "last-activity",
        [](const QJsonObject &project, int role) -> QVariant {
            if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
                return project["last_activity_at"].toVariant();
            }
            if (role == Roles::DATETIME) {
                return QDateTime::fromString(project["last_activity_at"].toString(),
                                             Qt::DateFormat::ISODateWithMs).toLocalTime();
            }
            return QVariant();
        });

    assert(column_index("id") == Columns::ID);
    assert(column_index("name") == Columns::NAME);
    assert(column_index("name_with_namespace") == Columns::NAME_WITH_NAMESPACE);
    assert(column_index("path_with_namespace") == Columns::PATH_WITH_NAMESPACE);
    assert(column_index("avatar_url") == Columns::AVATAR_URL);
    assert(column_index("description") == Columns::DESCRIPTION);
    assert(last_activity_column == Columns::LAST_ACTIVITY);
}
