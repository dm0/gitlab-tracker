#include "popupview.h"

PopupView::PopupView(QWidget *parent):
    QListView(parent)
{
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
}

int PopupView::sizeHintForRow(int row) const
{
    return sizeHintForIndex(model()->index(row, modelColumn())).height();
}
