#ifndef ASYNCNOTESMODEL_H
#define ASYNCNOTESMODEL_H

#include "asyncjsonmodel.h"

class AsyncNotesModel: public AsyncJsonModel {
    Q_OBJECT
public:
    /**
     * Constants for column indices.
     * @note Columns struct acts as a namespace for the inner anonymous unscoped enum.
     * The unscoped enum us used as Qt interfaces accept column index as int.
     */
    struct Columns {
        enum {
            ID = 0, /**< Note ID */
            BODY = 1, /**< Note text */
            ATTACHMENT = 2, /**< Note attachment */
            SYSTEM = 3, /**< If note is system or not */
            RESOLVABLE = 4, /**< If note is resolvable */
            /* Columns below are computed columns */
            CREATED_AT = 5, /**< Note create time, roles: +DATETIME */
            TIME_VALUE = 6, /**< Time value (only if time action > 0), roles: +NUMERIC */
            TIME_ACTION = 7  /**< Time action column, roles: +NUMERIC */
        };
    };
    /**
     * Data roles supported by some columns of the model.
     * @note Roles struct acts as a namespace for the inner anonymous unscoped enum.
     * The unscoped enum us used as Qt interfaces accept data role as int.
     */
    struct Roles {
        enum {
            DATETIME = Qt::UserRole, /**< Return DateTime value */
            NUMERIC /**< Return numeric value (int) */
        };
    };

    /**
     * Construct notes model for the passed async list of JSON objects (must contain notes).
     * Can be constructed with empty list. List can be set any time.
     * @param objlist List of notes.
     * @param parent Parent QObject.
     */
    AsyncNotesModel(gitlabclient::AsyncJsonObjList *objlist=nullptr,
                    QObject *parent=nullptr);

private:
    static const QStringList json_fields; /**< List of fields served by the model */

};

#endif // ASYNCNOTESMODEL_H
