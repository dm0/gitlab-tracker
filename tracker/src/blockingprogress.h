#ifndef BLOCKINGPROGRESS_H
#define BLOCKINGPROGRESS_H

#include <QWidget>

namespace Ui {
class BlockingProgress;
}

/**
 * Blocking widget with a progress bar in continuous mode.
 */
class BlockingProgress: public QWidget {
    Q_OBJECT

public:
    explicit BlockingProgress(QWidget *parent=nullptr);
    ~BlockingProgress();

public slots:
    /**
     * Set label text.
     * @param text New label text.
     */
    void set_message_text(const QString &text);

private:
    Ui::BlockingProgress *ui;
};

#endif // BLOCKINGPROGRESS_H
