#include "blockingprogress.h"
#include "ui_blockingprogress.h"

BlockingProgress::BlockingProgress(QWidget *parent):
    QWidget(parent),
    ui(new Ui::BlockingProgress)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint);
    setWindowModality(Qt::WindowModality::WindowModal);
}

BlockingProgress::~BlockingProgress()
{
    delete ui;
}

void BlockingProgress::set_message_text(const QString &text)
{
    ui->label->setText(text);
}
