#include <QStringList>
#include <QStringBuilder>
#include <QRegularExpression>

#include <QDebug>

#include "timeutils.h"

namespace timeutils {


QString format_time(long seconds, const QString &zero, long max_seconds)
{
    // Check early to skip next part.
    if (!seconds) {
        return zero;
    }

    QStringList result;
    result.reserve(6);
    // If max_seconds is not zero and seconds above, then seconds not shown and then we need to
    // round value to closest minutes. Adding half of a minute to get rounding behavior.
    seconds += max_seconds && (seconds > max_seconds) ? seconds_per_min / 2 : 0;
    long months = seconds / seconds_per_month;
    long weeks = seconds % seconds_per_month / seconds_per_week;
    long days = seconds % seconds_per_week / seconds_per_day;
    long hours = seconds % seconds_per_day / seconds_per_hour;
    long minutes = seconds % seconds_per_hour / seconds_per_min;
    seconds = max_seconds && (seconds > max_seconds) ? 0 : seconds % seconds_per_min;

    // Need to check again as seconds can become zero if seconds were suppressed.
    if (!(seconds || minutes || hours || days || weeks || months)) {
        return zero;
    }

    if (months) {
        result += QString::number(months) + "mo";
    }
    if (weeks) {
        result += QString::number(weeks) + "w";
    }
    if (days) {
        result += QString::number(days) + "d";
    }
    if (hours) {
        result += QString::number(hours) + "h";
    }
    if (minutes) {
        result += QString::number(minutes) + "m";
    }
    if (seconds) {
        result += QString::number(seconds) + "s";
    }
    return result.join(" ");
}


std::pair<GitlabTimeAction, long> parse_gitlab_time_note(const QString &time_str)
{
    static QRegularExpression re{
        R"/(^(?<action>added|subtracted|changed time estimate to) )/"
        R"/(((?<months>\d+)mo)?\s*((?<weeks>\d+)w)?\s*((?<days>\d+)d)?\s*((?<hours>\d+)h)?\s*)/"
        R"/(((?<minutes>\d+)m)?\s*((?<seconds>\d+)s)?( of time spent at \d{4}-\d{2}-\d{2}$)?)/",
        QRegularExpression::CaseInsensitiveOption | QRegularExpression::OptimizeOnFirstUsageOption
    };
    QRegularExpressionMatch match = re.match(time_str);
    if (!match.hasMatch()) {
        qWarning() << "Failed to parse time string: " << time_str;
        return {GitlabTimeAction::Unknown, 0};
    }
    int time = match.capturedRef("months").toInt() * seconds_per_month +
               match.capturedRef("weeks").toInt() * seconds_per_week +
               match.capturedRef("days").toInt() * seconds_per_day +
               match.capturedRef("hours").toInt() * seconds_per_hour +
               match.capturedRef("minutes").toInt() * seconds_per_min +
               match.capturedRef("seconds").toInt();
    if (match.capturedRef("action") == "added") {
        qDebug().nospace() << "Parsed " << time_str << " ⟶ +" << time;
        return {GitlabTimeAction::Added, time};
    }
    if (match.capturedRef("action") == "subtracted") {
        qDebug().nospace() << "Parsed " << time_str << " ⟶ -" << time;
        return {GitlabTimeAction::Subtracted, time};
    }
    qDebug().nospace() << "Parsed " << time_str << " ⟶ ~" << time;
    return {GitlabTimeAction::Estimated, time};
}

long parse_time_string(const QString &time_str, long single_number_seconds)
{
    static QRegularExpression re{
        R"/(^\s*(()/"
        R"/(((?<months>\d+)(mo|mon|month|months))?\s*)/"
        R"/(((?<weeks>\d+)(w|week|weeks|wks))?\s*)/"
        R"/(((?<days>\d+)(d|day|days))?\s*)/"
        R"/(((?<hours>\d+)(h|hour|hours|hrs))?\s*)/"
        R"/(((?<minutes>\d+)(m|min|mins|minutes))?\s*)/"
        R"/(((?<seconds>\d+)(s|sec|seconds))?)/"
        R"/()|()/"
        R"/(?<singlenum>\d+)/"
        R"/())\s*$)/",
        QRegularExpression::CaseInsensitiveOption | QRegularExpression::OptimizeOnFirstUsageOption
    };
    QRegularExpressionMatch match = re.match(time_str);
    if (!match.hasMatch()) {
        return -1;
    }
    if (!match.capturedRef("singlenum").isNull()) {
        if (single_number_seconds == 0) {
            return -1;
        }
        return match.capturedRef("singlenum").toInt() * single_number_seconds;
    }
    long time = match.capturedRef("months").toInt() * seconds_per_month +
                match.capturedRef("weeks").toInt() * seconds_per_week+
                match.capturedRef("days").toInt() * seconds_per_day +
                match.capturedRef("hours").toInt() * seconds_per_hour +
                match.capturedRef("minutes").toInt() * seconds_per_min +
                match.capturedRef("seconds").toInt();

    return time;
}

QString format_time_hours(long seconds, const QString &zero, long max_seconds)
{
    if (!seconds) {
        return zero;
    }

    QStringList result;
    result.reserve(3);

    long hours = seconds / seconds_per_hour;
    long minutes = seconds % seconds_per_hour / seconds_per_min;
    seconds = max_seconds && (seconds > max_seconds) ? 0 : seconds % seconds_per_min;
    if (hours) {
        result += QString::number(hours) + "h";
    }
    if (minutes) {
        result += QString::number(minutes) + "m";
    }
    if (seconds) {
        result += QString::number(seconds) + "s";
    }
    return result.join(" ");
}

} // namespace timeutils
