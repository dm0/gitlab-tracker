#ifndef ASYNCPROJECTSMODEL_H
#define ASYNCPROJECTSMODEL_H

#include "asyncjsonmodel.h"

/**
 * Asynchronous projects model class.
 *
 * A thin wrapper around AsyncJsonModel to provide column constants and hide list of fields.
 */
class AsyncProjectsModel: public AsyncJsonModel {
    Q_OBJECT
public:
    /**
     * Constants for column indices.
     * @note Columns struct acts as a namespace for the inner anonymous unscoped enum.
     * The unscoped enum us used as Qt interfaces accept column index as int.
     */
    struct Columns {
        enum {
            ID = 0, /**< Project ID */
            NAME = 1, /**< Project name */
            NAME_WITH_NAMESPACE = 2, /**< Project name prefixed with namespace */
            PATH_WITH_NAMESPACE = 3, /**< Project path with namespace */
            AVATAR_URL = 4, /**< Project avatar URL */
            DESCRIPTION = 5, /**< Project description */
            LAST_ACTIVITY = 6 /**< Last project activity */
        };
    };

    /**
     * Data roles supported by some columns of the model.
     * @note Roles struct acts as a namespace for the inner anonymous unscoped enum.
     * The unscoped enum us used as Qt interfaces accept data role as int.
     */
    struct Roles {
        enum {
            DATETIME = Qt::UserRole /**< Return DateTime value */
        };
    };

    /**
     * Construct projects model for the passed async list of JSON objects (must contain projects).
     * Can be constructed with empty list. List can be set any time.
     * @param objlist List of projects.
     * @param parent Parent QObject.
     */
    AsyncProjectsModel(gitlabclient::AsyncJsonObjList *objlist=nullptr,
                       QObject *parent=nullptr);

private:
    static const QStringList json_fields; /**< List of fields served by the model */
};

#endif // ASYNCPROJECTSMODEL_H
