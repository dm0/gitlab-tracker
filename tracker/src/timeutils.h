#ifndef TIMEUTILS_H
#define TIMEUTILS_H

#include <QString>

namespace timeutils {

// Enumeration of parsed time actions from Gitlab system note
enum GitlabTimeAction {
    Added, /**< Time added */
    Subtracted, /**< Time subtracted */
    Estimated, /**< Time estimation set / updated */
    Unknown /**< Unknown action / not a time action */
};

/**
 * @name Time constants
 * Constants define periods of time used to format messages.
 * Currently they correspond to Gitlab constants.
 */
/**@{*/
static constexpr long seconds_per_min = 60;
static constexpr long seconds_per_hour = seconds_per_min * 60;
static constexpr long seconds_per_day = seconds_per_hour * 8;
static constexpr long seconds_per_week = seconds_per_day * 5;
static constexpr long seconds_per_month = seconds_per_week * 4;
/**@}*/

/**
 * Format time given in seconds to display time tracking.
 * Formats time this way:
 * Mmo Ww Dd Hh Mm [Ss]
 * @param seconds Time in seconds.
 * @param zero String to return if time is zero
 * @param max_seconds Hide seconds automatically if total seconds is big.
 * @return Formatted time
 */
QString format_time(long seconds, const QString &zero="", long max_seconds=0);

/**
 * Format time given in seconds as hours.
 * Formats time this way:
 * Hh Mm [Ss]
 * @param seconds Time in seconds.
 * @param zero String to return if time is zero
 * @param max_seconds Hide seconds automatically if total seconds is big.
 * @return Formatted time string
 */
QString format_time_hours(long seconds, const QString &zero="", long max_seconds=0);

/**
 * Parses Gitlab system not into time action.
 * @param time_str Gitlab note body string.
 * @return Pair of time action and time (seconds)
 */
std::pair<GitlabTimeAction, long> parse_gitlab_time_note(const QString &time_str);

/**
 * Parse time string.
 * @param time_str Time string to parse.
 * @param single_number_seconds Enable parse of a string containing only number and parse it as
 * the multiplier of the passed number of seconds. Zero disables this functionality.
 * For example single_number_seconds=timeutils::seconds_per_hour to parse string "2" as two hours.
 * @return Parsed time in seconds. Negative if parse failed.
 */
long parse_time_string(const QString &time_str, long single_number_seconds=0);


} // namespace timeutils

#endif // TIMEUTILS_H
