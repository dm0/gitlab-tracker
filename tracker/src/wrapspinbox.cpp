#include "wrapspinbox.h"



WrapSpinBox::WrapSpinBox(QWidget *parent): QSpinBox(parent)
{

}

void WrapSpinBox::stepBy(int steps)
{
    int next = value() + steps * singleStep();
    QSpinBox::stepBy(steps);
    if (next > maximum()) {
        emit wrapped(1);
    } else if (next < minimum()) {
        emit wrapped(-1);
    }

}
