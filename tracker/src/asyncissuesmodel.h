#ifndef ASYNCISSUESMODEL_H
#define ASYNCISSUESMODEL_H

#include <QDir>

#include "asyncjsonmodel.h"

/**
 * Asynchronous issues model class.
 *
 * A thin wrapper around AsyncJsonModel to provide column constants and hide list of fields and
 * to provide some column values as specific data types.
 */
class AsyncIssuesModel: public AsyncJsonModel {
    Q_OBJECT
public:
    /**
     * Constants for column indices.
     * @note Columns struct acts as a namespace for the inner anonymous unscoped enum.
     * The unscoped enum us used as Qt interfaces accept column index as int.
     */
    struct Columns {
        enum {
            ID = 0, /**< Issue ID */
            IID = 1, /**< Internal issue ID (issue number) */
            TITLE = 2, /**< Issue title */
            STATE = 3, /**< Issue state */
            DESCRIPTION = 4, /**< Issue description */
            PROJECT_ID = 5, /**< Project ID */
            UPDATED_AT = 6, /**< The last issue update, roles: +DATETIME */
            NUM_AND_TITLE = 7, /**< Issue number and title as a string */
            TIME_SPENT = 8, /**< Issue time spent, roles: +NUMERIC */
            TIME_ESTIMATE = 9 /**< Issue time estimate, roles: +NUMERIC */
        };
    };
    /**
     * Data roles supported by some columns of the model.
     * @note Roles struct acts as a namespace for the inner anonymous unscoped enum.
     * The unscoped enum us used as Qt interfaces accept data role as int.
     */
    struct Roles {
        enum {
            DATETIME = Qt::UserRole, /**< Return DateTime value */
            NUMERIC /**< Return numeric value (int) */
        };
    };

    /**
     * Construct issues model.
     * @param client Gitlab client instance.
     * @param parent Parent QObject.
     */
    AsyncIssuesModel(gitlabclient::Client *client, QObject *parent=nullptr);

    /**
     * Return reference to internal issues instance that can be used to set filtering options.
     * @return Internal issues reference. Use only to setup filter.
     */
    gitlabclient::Issues & filter() const { return *issues; }

public slots:
    /**
     * Load all issues.
     */
    void load_all_issues();

    /**
     * Load project issues.
     * @param id Project ID.
     */
    void load_project_issues(long id);

    /**
     * Load group issues.
     * @param id Group ID.
     */
    void load_group_issues(long id);

    /**
     * Enable or disable caching.
     * @param enable Pass true to enable cache and false to disable.
     */
    void set_cache_enabled(bool enable);

private:
    static const QStringList json_fields; /**< List of fields served by the model */

    // Hide `set_objects_list` as it not intended to be set externally.
    using AsyncJsonModel::set_objects_list;

    gitlabclient::Issues *issues; /**< issues instance */
    QDir cache_dir; /**< Cache directory */
    bool use_cache = false; /**< If cache is enabled */
};

#endif // ASYNCISSUESMODEL_H
