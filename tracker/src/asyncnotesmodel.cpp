#include <cassert>

#include "timeutils.h"

#include "asyncnotesmodel.h"

using timeutils::GitlabTimeAction;

const QStringList AsyncNotesModel::json_fields{
    "id", "body", "attachment", "system", "resolvable"
};

static void fill_cache(const QJsonObject &note, QVariantMap &cache)
{
    int time_sec;
    GitlabTimeAction act;
    std::tie(act, time_sec) = timeutils::parse_gitlab_time_note(note["body"].toString());

    cache["time-action"] = act;
    switch (act) {
        case GitlabTimeAction::Added:
            cache["time-action-str"] = "Added";
            break;

        case GitlabTimeAction::Subtracted:
            cache["time-action-str"] = "Subtracted";
            break;

        case GitlabTimeAction::Estimated:
            cache["time-action-str"] = "Estimated";
            break;

        default:
            cache["time-action-str"] = "";
    }

    cache["time-value"] = time_sec;
    cache["time-value-str"] = timeutils::format_time(std::abs(time_sec), "");

    QDateTime when = QDateTime::fromString(note["created_at"].toString(),
                                           Qt::ISODate).toLocalTime();
    cache["created-at"] = when;
    cache["created-at-str"] = when.toString(Qt::SystemLocaleShortDate);
}


AsyncNotesModel::AsyncNotesModel(gitlabclient::AsyncJsonObjList *objlist, QObject *parent):
    AsyncJsonModel(AsyncNotesModel::json_fields, objlist, parent)
{

    int created_at_column = add_computed_column(
        "created-at",
        [](const QJsonObject &note, int role, QVariantMap &cache) -> QVariant {
            if (!note["system"].toBool()) {
                return QVariant();
            }
            if (cache.isEmpty()) {
                fill_cache(note, cache);
            }
            if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
                return cache["created-at-str"];
            }
            if (role == Roles::DATETIME) {
                return cache["created-at"];
            }
            return QVariant();
        });


    int time_value_column = add_computed_column(
        "time-value",
        [](const QJsonObject &note, int role, QVariantMap &cache) -> QVariant {
            if (!note["system"].toBool()) {
                return QVariant();
            }
            if (cache.isEmpty()) {
                fill_cache(note, cache);
            }
            if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
                return cache["time-value-str"];
            }
            if (role == Roles::NUMERIC) {
                return cache["time-value"];
            }
            return QVariant();
        });


    int time_action_column = add_computed_column(
        "time-action",
        [](const QJsonObject &note, int role, QVariantMap &cache) -> QVariant {
            if (!note["system"].toBool()) {
                return QVariant();
            }
            if (cache.isEmpty()) {
                fill_cache(note, cache);
            }
            if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) {
                return cache["time-action-str"];
            }
            if (role == Roles::NUMERIC) {
                return cache["time-action"];
            }

            return QVariant();
        });

    assert(column_index("id") == Columns::ID);
    assert(column_index("body") == Columns::BODY);
    assert(column_index("attachment") == Columns::ATTACHMENT);
    assert(column_index("system") == Columns::SYSTEM);
    assert(column_index("resolvable") == Columns::RESOLVABLE);

    assert(created_at_column == Columns::CREATED_AT);
    assert(time_value_column == Columns::TIME_VALUE);
    assert(time_action_column == Columns::TIME_ACTION);

}
