#ifndef ASYNCJSONMODEL_H
#define ASYNCJSONMODEL_H

#include <functional>

#include <QAbstractTableModel>

#include <gitlabclient/asyncjsonobjlist.h>

/**
 * Model for Gitlab projects list (Model / View framework).
 */
class AsyncJsonModel: public QAbstractTableModel {
    Q_OBJECT

public:
    using ComputeColumnFn = QVariant(const QJsonObject &, int role);
    using CachingComputeColumnFn = QVariant(const QJsonObject &, int role, QVariantMap &cache);
    /**
     * Construct model with the given parent and AsyncJsonObjList.
     * Model takes ownership of the objects list.
     * It is required to provide list of in the data with `set_columns` before this model
     * can be used (otherwise it will have zero columns).
     * @param objlist Async list of objects to be served by the model.
     * @param parent Parent object.
     */
    explicit AsyncJsonModel(gitlabclient::AsyncJsonObjList *objlist=nullptr,
                            QObject *parent=nullptr);

    /**
     * Construct model with the given parent and AsyncJsonObjList.
     * Model takes ownership of the objects list.
     * This constructor accepts list of fields of the objects to be served as columns.
     * @param columns Field names of the objects. The order defines column numbers.
     * @param objlist Async list of objects to be served by the model.
     * @param parent Parent object
     */
    explicit AsyncJsonModel(const QStringList &columns,
                            gitlabclient::AsyncJsonObjList *objlist=nullptr,
                            QObject *parent=nullptr);


    /**
     * Reimplemented function, returns count of objects.
     * @param parent Parent index.
     * @return Number of served objects.
     */
    int rowCount(const QModelIndex &parent=QModelIndex()) const override;

    /**
     * Reimplemented function, returns count of (known) columns.
     * @param parent Parent index.
     * @return Number of known columns.
     */
    int columnCount(const QModelIndex &parent=QModelIndex()) const override;

    /**
     * Reimplemented function, returns item data.
     * @param index Item index.
     * @param role Item role.
     * @return Item data.
     */
    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;

    /**
     * Reimplemented function, returns header data
     * @param section Column / row number
     * @param orientation Orientation
     * @param role Data role
     * @return Column name of row number
     */
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role=Qt::DisplayRole) const override;


    /**
     * Returns current status of updating operation.
     * @return True if currently updating list of the projects, false otherwise.
     */
    bool is_updating();

    /**
     * Returns list of the served columns. The list includes computed columns.
     * @return List of the known columns.
     */
    QStringList columns() const;

    /**
     * Add a column that will have a value computed with the provided function.
     * @param name Column name. Must be unique.
     * @param compute_fn Function to use for the computation.
     * @return Index of the added column.
     */
    int add_computed_column(const QString &name, std::function<ComputeColumnFn> compute_fn);

    /**
     * Overloaded version that takes a function supporting caching.
     * @param name Column name
     * @param compute_fn Caching compute function.
     * @return Index of the added column.
     *
     * @overload
     */
    int add_computed_column(const QString &name, std::function<CachingComputeColumnFn> compute_fn);

    /**
     * Return model index for the given row and column name.
     * This overloaded version takes column name as a string for convenience.
     * @param row Row index.
     * @param column Column name.
     * @return Model index.
     */
    QModelIndex index(int row, const QString &column);

    // Pull index overloads from the base
    using QAbstractTableModel::index;

    /**
     * Returns column index by name.
     * Returns -1 if the name is unknown.
     * @param column Column name.
     * @return Column index or -1.
     */
    int column_index(const QString &column) const { return column_names_map.value(column, -1); }

public slots:
    /**
     * Reload projects.
     */
    void refresh();

    /**
     * Set objects list to be served by the model.
     * Model takes ownership of the list. It also initiates fetching all async results.
     * The previous list will be deleted via `deleteLater`.
     * @param objlist New list of objects.
     */
    void set_objects_list(gitlabclient::AsyncJsonObjList *objlist);

    /**
     * Set names of the column in the underlying data to be served by the model.
     * Order of the names defines order of the columns.
     * Call to this function resets all registered computed columns.
     * @param columns Names of the columns (fields) in the data.
     */
    void set_columns(const QStringList &columns);

    /**
     * Set column title (as returned by headerData).
     * @param index Column index.
     * @param title Column title.
     */
    void set_column_title(int index, const QString &title);

    /**
     * Set cache merge key.
     *
     * Set the key used to merge cached values with the loaded objects.
     * The key must exist both in cached and in loaded JSON objects.
     *
     * @param key Key (field) name.
     */
    void set_cache_key(const QString &key) { cache_key = key; }

    /**
     * Load cache from file and merge it with the current objects list using current cache key.
     *
     * Returns QDateTime object with the date and time when the loaded cache was saved.
     * The merged list is written back to the `file_path`.
     *
     * @param file_path Cache file path.
     * @return QDateTime object with the last cache save time.
     */
    QDateTime set_cache_path(const QString &file_path);

    /**
     * Preload cache from the file.
     *
     * The loaded cache will be used after the next object list update (either refresh or set).
     * The effect is the same as if the `set_cache_path(file_path)` was called immediately after
     * `refresh` or `set_objects_list`.
     *
     * @param file_path Cache file path.
     * @return QDateTime object with the last cache save time.
     */
    QDateTime preload_cache(const QString &file_path);

signals:
    /**
     * Emitted before starting network request.
     */
    void update_started();

    /**
     * Emitted after network request finished.
     */
    void update_finished();

    /**
     * Emitted when network request started or finished with corresponding value.
     * @param updating True if update started and false if finished.
     */
    void updating(bool updating);

    /**
     * Re-emit low-level `failed` signal of `AsyncJsonResponse`.
     * @param error Network error.
     */
    void failed(QNetworkReply::NetworkError error);

private slots:
    /**
     * Slot called as the result of loading all async data.
     */
    void data_loaded();

private:
    struct ColumnInfo {
        enum class Kind {Data, Computed, Caching} kind;
        int index;
        QString name;
    };
    /**
     * Populate map of usual columns
     * @param columns List of data columns.
     */
    void populate_columns(const QStringList &columns);

    /**
     * Read cache from file into preloaded cache values.
     * @param path Cache file path
     * @return DateTime object with the time the cache was saved.
     */
    QDateTime read_cache_file(const QString &path);

    /**
     * Merge preloaded cache with remote object to create local cache.
     */
    void merge_preloaded_cache();

    /**
     * Save cache into file.
     * File contents will be overwritten with the current local cache values and remote objects.
     * @param path Cache file path.
     */
    void save_cache(const QString &path);

    gitlabclient::AsyncJsonObjList *objects = nullptr; /**< Async list of objects */
    QVector<ColumnInfo> _columns; /**< Columns served by the model */
    QVector<std::function<ComputeColumnFn>> _computed_columns; /**< Computed columns */
    /** Computed columns that support caching */
    QVector<std::function<CachingComputeColumnFn>> _caching_computed_columns;
    QHash<QString, int> column_names_map; /**< Column names to index mapping */
    mutable QMap<int, QVariantMap> columns_cache; /**< Cache */
    QStringList titles; /**< Column titles */
    QVector<QJsonObject> cache; /**< Local cache to be served with objects */
    /** Preloaded cache, will be merged on next objects list update */
    QVector<QJsonObject> preloaded_cache;
    QString cache_key; /**< Cache merging key */
    QString cache_path; /**< Cache file path (to write back) */
};

#endif // ASYNCJSONMODEL_H
