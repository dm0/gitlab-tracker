#include <QDebug>

#include <QDir>
#include <QKeySequence>
#include <QStandardPaths>

#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include "appsettings.h"

namespace defaults = settings::defaults;
namespace keys = settings::keys;


SettingsDialog::SettingsDialog(QWidget *parent):
    QDialog(parent),
    ui(new Ui::SettingsDialog),
    settings(new QSettings(this))
{
    ui->setupUi(this);
    // Connect accepted signal to settings saving
    connect(this, &SettingsDialog::accepted, this, &SettingsDialog::save_settings);
    // Connect button box clicked signal to the handling slot.
    connect(ui->buttonBox, &QDialogButtonBox::clicked,
            this, &SettingsDialog::handle_buttonbox_button);
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

int SettingsDialog::exec()
{
    load_settings();
    return QDialog::exec();
}

void SettingsDialog::open()
{
    load_settings();
    return QDialog::open();
}

void SettingsDialog::load_settings()
{
    qDebug() << "Loading settings to UI";
    ui->gitlab_endpoint->setText(
        settings->value(keys::instance_url, defaults::instance_url).toString());
    ui->sync_delay->setValue(settings->value(keys::sync_delay, defaults::sync_delay).toInt());
    ui->sync_remove_delay->setValue(
        settings->value(keys::sync_remove_delay, defaults::sync_remove_delay).toInt());
    ui->recent_issues_count->setValue(
        settings->value(keys::recent_issues_count, defaults::recent_issues_count).toInt());
    ui->recent_projects_count->setValue(
        settings->value(keys::recent_projects_count, defaults::recent_projects_count).toInt());
    ui->overdue_notification_time->setValue(
        settings->value(keys::overdue_notif_time, defaults::overdue_notif_time).toInt());
    ui->overdue_notification_set_estimate->setChecked(
        settings->value(keys::overdue_notif_set_estimate,
                        defaults::overdue_notif_set_estimate).toBool());
    // ui->enable_local_cache->setChecked(
    //     settings->value(keys::enable_cache,
    //                     defaults::enable_cache).toBool());

    // Shortcuts.
    ui->toggle_tracking_sequence->setKeySequence(
        settings->value(keys::toggle_tracking_shortcut,
                        defaults::toggle_tracking_shortcut).value<QKeySequence>()
        );
    ui->toggle_window_sequence->setKeySequence(
        settings->value(keys::toggle_appwindow_shortcut,
                        defaults::toggle_appwindow_shortcut).value<QKeySequence>()
        );
    ui->focus_projects_sequence->setKeySequence(
        settings->value(keys::focus_projects_shortcut,
                        defaults::focus_projects_shortcut).value<QKeySequence>()
        );
    ui->focus_issues_sequence->setKeySequence(
        settings->value(keys::focus_issues_shortcut,
                        defaults::focus_issues_shortcut).value<QKeySequence>()
        );
}

void SettingsDialog::save_settings()
{
    qDebug() << "Saving settings from UI";
    settings->setValue(keys::instance_url, ui->gitlab_endpoint->text());
    settings->setValue(keys::sync_delay, ui->sync_delay->value());
    settings->setValue(keys::sync_remove_delay, ui->sync_remove_delay->value());
    settings->setValue(keys::recent_issues_count, ui->recent_issues_count->value());
    settings->setValue(keys::recent_projects_count, ui->recent_projects_count->value());
    settings->setValue(keys::overdue_notif_time, ui->overdue_notification_time->value());
    settings->setValue(keys::overdue_notif_set_estimate,
                       ui->overdue_notification_set_estimate->isChecked());
    // settings->setValue(keys::enable_cache, ui->enable_local_cache->isChecked());

    // Shortcuts
    settings->setValue(keys::toggle_tracking_shortcut,
                       ui->toggle_tracking_sequence->keySequence().toString());
    settings->setValue(keys::toggle_appwindow_shortcut,
                       ui->toggle_window_sequence->keySequence().toString());
    settings->setValue(keys::focus_projects_shortcut,
                       ui->focus_projects_sequence->keySequence().toString());
    settings->setValue(keys::focus_issues_shortcut,
                       ui->focus_issues_sequence->keySequence().toString());
}

void SettingsDialog::reset()
{
    ui->gitlab_endpoint->setText(defaults::instance_url);
    ui->sync_delay->setValue(defaults::sync_delay);
    ui->sync_remove_delay->setValue(defaults::sync_remove_delay);
    ui->recent_issues_count->setValue(defaults::recent_issues_count);
    ui->recent_projects_count->setValue(defaults::recent_projects_count);
    ui->overdue_notification_time->setValue(defaults::overdue_notif_time);
    ui->overdue_notification_set_estimate->setChecked(defaults::overdue_notif_set_estimate);

    // Set shortcuts to their defaults.
    ui->toggle_tracking_sequence->setKeySequence({defaults::toggle_tracking_shortcut});
    ui->toggle_window_sequence->setKeySequence({defaults::toggle_appwindow_shortcut});
    ui->focus_projects_sequence->setKeySequence({defaults::focus_projects_shortcut});
    ui->focus_issues_sequence->setKeySequence({defaults::focus_issues_shortcut});
}

void SettingsDialog::handle_buttonbox_button(QAbstractButton *button)
{
    if (ui->buttonBox->buttonRole(button) != QDialogButtonBox::ResetRole) {
        return;
    }
    reset();
}

void SettingsDialog::clear_caches()
{
    qDebug() << "Cleaning up application cache directory";
    QDir{QStandardPaths::writableLocation(QStandardPaths::CacheLocation)}.removeRecursively();
}
