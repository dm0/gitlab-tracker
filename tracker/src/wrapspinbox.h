#ifndef WRAPSPINBOX_H
#define WRAPSPINBOX_H

#include <QObject>
#include <QSpinBox>

/**
 * QSpinBox that emits additional values on value wrapping.
 */
class WrapSpinBox: public QSpinBox
{
    Q_OBJECT

public:
    /**
     * Construct WrapSpinBox
     * @param parent Parent widget
     */
    WrapSpinBox(QWidget *parent=nullptr);
    /**
     * Reimplemented from the QSpinBox to emit wrapped_up/down signals.
     * Calls base implementation and emits signals in case of value wrap.
     * @param steps Number of steps.
     */
    virtual void stepBy(int steps) override;
signals:
    /**
     * The signal is emitted when the value has wrapped.
     * @param direction Wrap direction: +1 if wrapped down (from maximum() to minimum()),
     * -1 otherwise.
     */
    void wrapped(int direction);
};

#endif // WRAPSPINBOX_H
