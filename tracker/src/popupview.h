#ifndef POPUPVIEW_H
#define POPUPVIEW_H

#include <QListView>

/**
 * QListView subclass that correctly computes row height to be used with QCompleter popups.
 * Original QListView uses all columns to compute row height.
 * It results in a wrong popup size if items contain new lines.
 */
class PopupView: public QListView
{
public:
    /**
     * Construct PopupView with the given parent.
     * Calls base constructor and performs the following setup:
     * - setEditTriggers(QAbstractItemView::NoEditTriggers);
     * - setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
     * - setSelectionBehavior(QAbstractItemView::SelectRows);
     * - setSelectionMode(QAbstractItemView::SingleSelection);
     * @param parent Parent widget
     */
    PopupView(QWidget *parent = nullptr);
protected:

    /**
     * Reimplemented to use only modelColumn for height calculation.
     * @param row Row index.
     * @return Row height.
     */
    int sizeHintForRow(int row) const override;

};

#endif // POPUPVIEW_H
