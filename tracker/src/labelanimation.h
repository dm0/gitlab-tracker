#ifndef LABELANIMATION_H
#define LABELANIMATION_H

#include <QObject>
#include <QMovie>
#include <QLabel>

class LabelAnimation : public QObject
{
    Q_OBJECT
public:
    explicit LabelAnimation(const QString &animation_file, QObject *parent = nullptr);
    void animate(QLabel * target);

signals:

public slots:
private:
    void start_animation();
    void stop_animation();
    bool eventFilter(QObject *obj, QEvent *event) override;

    QMovie * movie;
    int start_count = 0;
};

#endif // LABELANIMATION_H
