RELEASE_NOTES="$(awk -f getlastchanges.awk CHANGELOG.md)"

for name in $(ls -1 debs/*/*); do
  ASSET=$(jq -nc --arg link "$CI_JOB_URL/artifacts/raw/$name" --arg name "${name#*/}" '{name: $name, url: $link, link_type: "package"}')
  ASSET_LINKS+=(--assets-link "$ASSET")
  echo "Adding asset: $ASSET"
done

release-cli create --name "Release $CI_COMMIT_TAG" --description "$RELEASE_NOTES" "${ASSET_LINKS[@]}"
