TEMPLATE = subdirs

SUBDIRS += \
    gitlab-client-qt \
    tracker

gitlab-client-qt.subdir = third_parties/gitlab-client-qt

tracker.depends = gitlab-client-qt
