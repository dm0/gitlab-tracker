# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
New changes are added to this section first.


## [0.6.1] - 2020-12-27
### Fixed
* Fixed wrong icon theme on Linux. Previous version introduced a bug resulting in using shipped (non system) icons.

[0.6.1]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.6.0...v0.6.1


## [0.6.0] - 2020-09-19
### Added
* Added build support for macOS.
* Bundled used icons as a built-in theme.
* Renamed some icons to achieve broader compatibility with standard icon names.
* Hiding Dock icon on Mac when hiding the application to systray.

### Fixed
* Fixed crash in case of global hotkeys registration failure (mac).
* Fixed saving of hotkeys configuration on mac.
* Fixed unintended window activation on single click on the systray icon.
* Fixed wrong remaining time reporting in the case time estimate is not set.
* Fixed recent issues menu display in case of current issue is not set.

### Removed
* Removed local issue cache support. Reliable implementation requires too many efforts and benefits are not that obvious except fort the very large projects with hundredth of open issues.

[0.6.0]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.5.1...v0.6.0


## [0.5.1] - 2020-06-02
### Added
* Added controls in settings dialog to disable and clear cache.

### Fixed
* Set cache disabled by default. This is a temporary workaround for the bug #60.

[0.5.1]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.5.0...v0.5.1


## [0.5.0] - 2020-01-21
### Added
* Tray icon now displays tooltips with information about current project, issue
  and time tracking on supported platforms (doesn't work on Ubuntu 18.04).
* Issues are now cached locally to speedup loading of issues in big projects.

### Changed
* Local time records got additional state "Sending". Displayed instead of 
  "Local (0)".
* Improved controls tab order. Removed reload buttons from the tab list.
* Last active issue is now stored for each project.

[0.5.0]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.4.2...v0.5.0


## [0.4.2] - 2019-12-12
### Fixed
* Fixed local time records TTS timer update slowly.

[0.4.2]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.4.1...v0.4.2


## [0.4.1] - 2019-12-10
### Added
* Added checkbox in settings dialog to control if overdue notification click
  should suggest time estimate update.  
  UI also has a notice about the feature may not work with some configurations.

### Changed
* Overdue notification click action is now disabled by default.

[0.4.1]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.4.0...v0.4.1


## [0.4.0] - 2019-12-08
### Fixed
* Text cursor in issue selection combo now moves to beginning of the text after
  issue selection.
* Fixed application exit when closing about dialog if main window not visible.

### Added
* Added settings dialog (accessible via tray menu).
* Time entry dialog now displays information about current project and issue.
* Implemented hotkeys to set focus to important controls and global hotkeys
  to toggle time tracking and window state.
* Added recent projects dropdown menu:
  - The menu is available as a submenu of current project.
  - Added recent project count option to application settings dialog.
* Added settings to control overdue notification time. Value of 0 disables
  notifications.
* Click on overdue notification now suggests estimate update on supported 
  platforms.  
  Looks like it doesn't work on Ubuntu 18.04 for some reason.

[0.4.0]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.3.0...v0.4.0


## [0.3.0] - 2019-10-14
### Fixed
* Fixed time tracking UI kept enabled state if active issue disappears from 
  the list.
* Time tracking UI's remaining / overdue time now doesn't display seconds. 
  This solves issue about update resolution.

### Added
* Time records view split is now adjustable. Proportion between local and 
  remote time records can be set by dragging.
* Time records now preserve their state. Window height and split state are 
  preserved. This works both between application restarts and toggling records
  visibility within one session.

[0.3.0]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.2.0...v0.3.0


## [0.2.0] - 2019-09-30
### Added
* Added about dialog. The dialog is accessible trough the system tray menu.
* Displaying TTS time for local time records in the status column.
* Added context menu action to sync local time records.
* TTS timer is now paused while local records context menu is open.

### Changed
* Improved records UI: columns order of remote records, single column with 
  project name and entity type and id for local records (instead of 3 separate 
  columns).

### Fixed
* Fixed application crash when a local time record was removed while auto-removal timer was active.

[0.2.0]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.1.0...v0.2.0


## [0.1.0] - 2019-09-08
### Added
* Time entry settings are now preserved between application restarts.
* Handling of a (rare) situation when an issue disappeared while tracking is active.
* Displaying balloon notification from systray icon 5 minutes before overdue.

### Fixed
* Fixed local time sync didn't result in remote records update starting from the second sync.
* Fixed update button sometimes resulting in a reset of current issue.

[0.1.0]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.0.3...v0.1.0


## [0.0.3] - 2019-08-23
### Added
* Text cursor is now positioned at the beginning of the issue/project title 
  after choosing an item in the dropdown list.
* Improved combo boxes UX: selecting all text on focus. This allows to quickly 
  change project issue without need to clear all text first.
* Added shortening of issue titles in systray menu to limit its width.
* Window size and position is now preserved between application restart and 
  window minimizing to systray.

### Changed
* Current issue information menu item replaced with a submenu with recent 
  issues (by the last update date).

### Fixed
* Fixed "overdue mode" of the progress bar. It didn't take into account local
  (not synced) time.

[0.0.3]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.0.2...v0.0.3


## [0.0.2] - 2019-08-11
### Fixed
* Progress bar now takes into account locally tracked (not synced) time (#1).
* Time input dialog of "Add manual time" action now opens with value of zero 
  instead of estimated time (#3).
* Free form text input field is now also updated on dialog open.
* Time formatting now rounds to nearest minute if requested to format time
  without seconds. This fixes issue with `estimate ≠ spent + remaining` (#4).
* Now tracking menu item now doesn't update its title while open. This fixes 
  issue with menu closes / collapses on title update (#2).

### Added
* Application now changes system tray icon to show current tracking state.  
  The added icon is based on the original icon but has bigger stopwatch and 
  smaller GitLab logo.

[0.0.2]: https://gitlab.com/dm0/gitlab-tracker/compare/v0.0.1...v0.0.2


## [0.0.1] - 2019-08-01

Initial implementation of the GitLab Tracker desktop application. With the 
following features:

* Support for GitLab authentication (oAuth).
* List projects available to the user.
* List issues of the selected project.
* View and set issue time estimate.
* Track issue time.
* Add manual issue time.
* View issue time records.
* Revert issue time records.
* Hide to system tray.

[0.0.1]: https://gitlab.com/dm0/gitlab-tracker/-/tags/v0.0.1
